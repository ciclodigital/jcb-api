module Jcb
  module Sisapo
    class Resultado
      attr_reader :id_reuniao
      attr_reader :numero
      attr_reader :estado
      attr_reader :resultados

      def initialize id_reuniao, numero, config={}
        @id_reuniao = id_reuniao
        @numero = numero
        @resultados = config[:resultados] || {}
        @estado = config[:estado] ||  nil #confirmado C = confirmado (podem pagar) J = em julgamento
      end

      def self.fetch id_reuniao, numero=0
        result = fetch_one id_reuniao, numero
        initialize_from_result(result)
      end

      def self.initialize_from_result result
        return nil if result.first.nil?

        self.new result.first[:idReuniao], result.first[:NumeroPareo], {
          favoritismo: result.first[:Favoritismo].split('-').join(' - '),
          estado: result.first[:Estado],
          resultados: self.format_resultados_from_result(result)
        }
      end

      def self.format_resultados_from_result result
        resultados = {}

        resultados[:tempo_vencedor] = result.first[:TempoDoVencedor]
        resultados[:ordem_chegada] = result.first[:OrdemDeChegada]
        resultados[:favoritismo] = result.first[:Favoritismo].split('-').join(' - ')
        
        result.each do |resultado|
          modalidade = nil

          case resultado[:tipoAposta]
            when "1"
              modalidade = :vencedor
            when "2"
              modalidade = :place
            when "3"
              modalidade = :dupla
            when "4"
              modalidade = :exata
            when "5"
              modalidade = :trifeta
            when "6"
              modalidade = :quadrifeta
          end

          resultados[modalidade] = [] if resultados[modalidade].nil?
          resultados[modalidade] << {
            :indicacao => resultado[:Indicacao],
            :complemento => resultado[:Complemento],
            :numero => resultado[:IndicacaoDescricao],
            :rateio => Util.convert_rateio_to_view(resultado[:Rateio]),
            :acrescimo => resultado[:Acrescimo]
          }
        end

        resultados
      end

      def resultado_para modalidade
        @resultados[modalidade]
      end

      private
        def self.fetch_one id_reuniao, numero=0
          result = nil
        
          Jcb::Sisapo::DB.do do |client|
            result = client.execute(%{
              EXEC [dbo].[pTvPareoResultado]
              @idReuniao = #{id_reuniao}
              #{", @numeroPareo = #{numero}" if numero > 0}
            })
            
            result.each(:symbolize_keys => true)
          end

          result
        end
    end
  end
end