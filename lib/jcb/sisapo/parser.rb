require 'titleize'
module Jcb
  module Sisapo
    module Parser
      MESSAGE_SEPARATOR   = "|"
      MESSAGE_SEPARATOR_1 = "^"
      MESSAGE_SEPARATOR_2 = "*"
      MESSAGE_SEPARATOR_3 = "#"
      MESSAGE_SEPARATOR_4 = "$"
      MODALIDADES = [:vencedor, :place, :dupla, :exata, :trifeta, :quadrifeta]

      ##
      # Converte a mensagem em Hash
      def self.parse type, txt
        return nil if txt.nil?
        self.send("parse_#{type}", txt)
      end

      private
        ##
        # Parseia a mensagem do tipo RA
        #
        # @example
        #
        #    <corpo> = <sigla hipo> <sep> <hipo> <sep> <reunião> <sep> <páreo> <sep>
        #    <informações do páreo> <sep> <flags prazo> <sep> <prazo> <sep> <vencedor> <sep> <placê>
        #    <sep> <dupla> <sep> <exata> <sep> <trifeta> <sep> <quadrifeta> <sep> <totais> <sep>
        #    <lideres> <sep> <msgRolante>
        #
        # @example
        #
        #    RA080815|142528|RJ|Gávea|38|1|GM^1000|101|FINAL|199##5#167#85441#*939##9#501#110811#*55##3#59#111981#*604##8#328#115541#*248##6#409#109211#*464##7#389#115551#*21##2#22#65101#*107##4#131#44131#*16##1#15#216021#^|72930*14430*141870*26710*46730*11150*250310*63450*264660|07-09#22*03-07#52*03-09#68*07-08#116*08-09#143*03-08#159*01-07#208*01-09#222*05-09#268*05-07#288*01-08#309*03-04#318*01-03#329*03-05#459*02-09#496*06-07#657|09-07#33*07-09#63*03-07#99*09-03#124*03-09#128*07-03#132*09-08#145*08-09#158*03-08#227*08-03#233*09-01#254*07-08#318*07-05#425*08-07#426*03-01#431*01-07#479|09-07-03#106*07-09-03#159*09-07-08#177*09-03-07#181*07-09-08#231*03-07-09#248*07-03-09#271*03-09-07#302*09-07-01#335*09-07-05#389*09-03-08#420*07-09-01#435*09-08-07#498*07-09-05#557*03-07-08#592*07-03-08#601|09-07-03-08#454*09-07-08-03#525*07-09-03-08#566*09-07-03-01#624*09-03-07-08#642*07-09-08-03#642*09-07-03-05#703*09-03-08-07#900*07-03-09-08#908*09-07-01-03#909*09-07-08-01#909*07-09-03-01#961*09-07-05-03#979*03-09-07-08#994*09-07-01-08#1012*09-03-07-01#1042|15186610^13597410^10619650#0#0*892240#0#0*511100#0#0*263960#0#0*495540#0#0*814920#0#0|Pick 7$Pick 7$Pick7$Pck7#1127800#0*^Betting 5$Bett 5$Bett5$Btt5#217600#3300000*^Super Betting$SupBETT$SpBet$SBet#243800#3000000*|
        #
        # @separador <sep> total de 15
        def self.parse_ra txt
          hash    = {}
          corpo   = txt.split(MESSAGE_SEPARATOR)
          i_pareo = corpo[6].split(MESSAGE_SEPARATOR_1)
          s_pareo = corpo[7].split("")
          vencedor = corpo[9].split(MESSAGE_SEPARATOR_2)
          place    = corpo[10].split(MESSAGE_SEPARATOR_2)
          
          hash[:estado]            = corpo[2]
          hash[:hipodromo]         = corpo[3]
          hash[:reuniao]           = corpo[4].to_i
          hash[:pareo]             = corpo[5].to_i
          hash[:pareo_tipo]        = i_pareo[0]
          hash[:pareo_distancia]   = i_pareo[1].to_i
          hash[:pareo_final]       = s_pareo[0] == "1"
          hash[:pareo_alinhamento] = s_pareo[1] == "1"
          hash[:pareo_placar]      = s_pareo[2] == "1"
          hash[:pareo_prazo]       = corpo[8]
          hash[:vencedor]          = []
          hash[:place]             = []
          hash[:dupla]             = self.parse_list(:ra_combinada, corpo[11].split(MESSAGE_SEPARATOR_2))
          hash[:exata]             = self.parse_list(:ra_combinada, corpo[12].split(MESSAGE_SEPARATOR_2))
          hash[:trifeta]           = self.parse_list(:ra_combinada, corpo[13].split(MESSAGE_SEPARATOR_2))
          hash[:quadrifeta]        = self.parse_list(:ra_combinada, corpo[14].split(MESSAGE_SEPARATOR_2))
          hash[:especiais]         = self.parse_ra_especiais(corpo[16])

          vencedor.each_with_index do |vencedor_txt, numero_animal|
            hash[:vencedor] << self.parse_ra_vencedor(numero_animal+1, vencedor_txt)
          end

          place.each_with_index do |vencedor_txt, numero_animal|
            hash[:place] << self.parse_ra_place(numero_animal+1, vencedor_txt)
          end

          not_has_place = true

          hash[:place].each do |place|
            unless place[:apostado] == '0,00'
              not_has_place = false
            end
          end

          hash[:place] = nil if not_has_place
          hash[:place] = hash[:place].compact unless hash[:place].nil?

          hash.merge(self.parse_ra_totais(corpo[15]))
        end

        def self.parse_list type, list
          result = list.map do |txt|
            self.send("parse_#{type}", txt)
          end

          if result.size > 0
            result.compact
          else
            nil
          end
        end

        ##
        # @example
        #
        #    "199##5#167#85441#"
        # 
        def self.parse_ra_vencedor numero, vencedor_txt
          slited = vencedor_txt.split(MESSAGE_SEPARATOR_3)

          {
            numero:  numero,
            rateio:  Util.convert_rateio_to_view(slited[0].to_i / 10.0),
            parelha: slited[1],
            favoritismo: slited[2].to_i,
            rateio_abertura: Util.convert_rateio_to_view(slited[3].to_i / 10.0),
            codigo_farda: slited[4],
            pick7_percente: slited[5]
          }
        end

        ##
        # @example
        #
        #    "72930"
        # 
        def self.parse_ra_place numero, place_txt
          {
            numero:  numero,
            apostado: Util.convert_number_to_view(place_txt.to_i / 100.0)
          }
        end

        ##
        # @example
        #
        #    "07-09#22"
        #    <combinacao>#<rateio>
        def self.parse_ra_combinada combinada_txt
          splited = combinada_txt.split(MESSAGE_SEPARATOR_3)
          if splited[0].empty?
            nil
          else
            {
              combinacao:  splited[0],
              rateio: Util.convert_rateio_to_view(splited[1].to_i / 10.0)
            }
          end
        end

        ##
        # @example
        #    "15186610^13597410^10619650#0#0*892240#0#0*511100#0#0*263960#0#0*495540#0#0*814920#0#0"
        #    <total_reuniao>^<total_pareo>^<total_modalidades></total_modalidades>
        def self.parse_ra_totais txt
          splited = txt.split(MESSAGE_SEPARATOR_1)
          total_reuniao = splited[0].to_i / 100.0
          total_pareo = splited[1].to_i / 100.0

          {
            total_reuniao: total_reuniao,
            total_pareo: total_pareo,
            total_reuniao_formatado: Util.convert_number_to_view(total_reuniao),
            total_pareo_formatado: Util.convert_number_to_view(total_pareo),
            total_modalidades: self.parse_ra_totais_modalidades(splited[2])
          }
        end

        def self.parse_ra_totais_modalidades txt
          modalidades = {}
          splited = txt.split(MESSAGE_SEPARATOR_2)

          splited.each_with_index do |modalidade, index|
            modalidade_splited = modalidade.split(MESSAGE_SEPARATOR_3)
            total = modalidade_splited[0].to_i / 100.0

            modalidades[MODALIDADES[index]] = {
              nome: MODALIDADES[index].to_s.titleize,
              total: total,
              total_formatado: Util.convert_number_to_view(total),
              acrescimo: Util.convert_number_to_view(modalidade_splited[1].to_i / 100.0),
              garantia: Util.convert_number_to_view(modalidade_splited[2].to_i / 100.0)
            }
          end

          modalidades
        end

        def self.parse_ra_especiais txt
          retorno = {}

          txt.split(MESSAGE_SEPARATOR_1).each do |partial|
            partial_splited = partial.split(MESSAGE_SEPARATOR_2)
            infos = partial_splited[0].split(MESSAGE_SEPARATOR_3)
            messages = partial.split(MESSAGE_SEPARATOR_2)[0].split(MESSAGE_SEPARATOR_4)
            
            acertos = []
            
            partial_splited.each_with_index do |value, key|
              unless key == 0
                temp = value.split(MESSAGE_SEPARATOR_3)
                acertos << {
                  acertos: temp[0],
                  soma_dos_rateios: Util.convert_rateio_to_view(temp[1].to_i / 10.0),
                  combinacoes: temp[2],
                  bilhetes: temp[3] 
                }
              end
            end

            retorno[messages[0].gsub(" ", "_").downcase] = {
              labels: infos[0].split(MESSAGE_SEPARATOR_4),
              total: Util.convert_number_to_view((infos[1].to_i / 100.0)),
              bonificacao: Util.convert_number_to_view((infos[2].to_i / 100.0)),
              acertos: acertos
            }
          end

          retorno
        end

    end
  end
end