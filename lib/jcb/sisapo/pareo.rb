module Jcb
  module Sisapo
    class Pareo
      attr_reader :id_reuniao, :numero, :rateios, :distancia, :pista, :status, :resultado, :extras, :forfaits, :animais, :craw_forfaits_text

      def initialize id_reuniao, numero, options={}
        @numero = numero
        @id_reuniao = id_reuniao
        @rateios = options[:rateios] || {}
        @especiais = get_rateio_especiais
        @distancia = options[:distancia]
        @pista = options[:pista]
        @status = options[:status] || :nao_definido
        @final = options[:final]
        @alinhamento = options[:alinhamento]
        @placar     = options[:placar]
        @resultado  = get_resultado
        @animais    = Jcb::Sisapo::Animal.fetch id_reuniao, numero
        @extras     = pareo_extras
        @forfaits   = get_forfaits
        @craw_forfaits_text = get_forfaits_texts
      end

      def custom_rateios
        news = {}
        
        [:vencedor, :place, :dupla, :exata, :trifeta, :quadrifeta].map do |key|
          columns = []
          
          if @rateios[key] && @rateios[key].any?
            @rateios[key].each_slice(6) do |group|
              columns  << group
            end
          end
          
          news[key] = columns
        end

        news
      end

      def animais_group
        animais = []
        
        @animais.each_slice(6) do |group|
          animais << group
        end

        animais
      end

      def self.fetch id_reuniao, numero=0
        result  = fetch_one(id_reuniao, numero)

        initialize_result(result)
      end

      def self.fetch_current id_reuniao=nil
        id_reuniao = self.fetch_next_one[:idReuniao].to_i if id_reuniao.nil?

        result  = fetch_one(id_reuniao, 0)

        initialize_result(result)
      end

      def self.fetch_next id_reuniao
        current_result = fetch_current(id_reuniao)
        result = fetch_one(current_result.id_reuniao, current_result.numero + 1)
        initialize_result(result)
      end

      def self.fetch_prev id_reuniao
        current = fetch_current(id_reuniao)

        return nil if current.numero == 1

        result = fetch_one(current.id_reuniao, current.numero - 1)

        initialize_result(result)
      end

      def self.initialize_result result
        return nil if result.nil?

        message = Jcb::Sisapo::Parser.parse(:ra, result[:mensagemRA]) unless result[:mensagemRA].nil? || result[:mensagemRA].empty?

        options = {
          distancia: result[:distancia].to_i,
          pista: result[:pista],
          rateios: message,
          status: result[:fechado] ? :fechado : :aberto, # apostas encerradas 1º
        };

        unless message.nil?
          options[:final] = message[:pareo_final] # Calculo final dos rateios 2º
          options[:alinhamento] = message[:pareo_alinhamento] # Alguns segundos antes de fechar
          options[:placar] = message[:pareo_placar]
        end

        self.new(result[:idReuniao], result[:pareoNumero], options)

        #RA: Mensagem de rateios
      end

      def final?
        @final
      end

      def alinhamento?
        @alinhamento
      end

      def placar?
        @placar
      end

      

      private
        def self.fetch_one id_reuniao, numero=0
          result = nil
        
          Jcb::Sisapo::DB.do do |client|
            result = client.execute(%{
              EXEC [dbo].[pTvPareoRateios]
              @idReuniao = #{id_reuniao}
              #{", @numeroPareo = #{numero}" if numero > 0}
            })
            
            result.each(:symbolize_keys => true)
          end

          result.first
        end

        def self.fetch_next_one
          result = nil
        
          Jcb::Sisapo::DB.do do |client|
            result = client.execute(%{
              EXEC [dbo].[piPareoProximo]
            })
            
            result.each(:symbolize_keys => true)
          end

          result.first
        end

        def get_resultado
          Resultado.fetch id_reuniao, numero
        end

        def pareo_extras
          result = []
          
          Jcb::Sisapo::DB.do do |client|
            result_db = client.execute(%{
              EXEC [dbo].[pTvPareos]
              @idReuniao = #{@id_reuniao}
            })
            
            result_db.each(:symbolize_keys => true)
            result_db.each do |row|
              result << {
                id_reuniao: row[:idReuniao],
                reuniao: row[:Reuniao],
                hipodromo: row[:Hipodromo],
                hipodromo_descricao: row[:HipodromoDescricao],
                dia: row[:Dia],
                quantidade_pareos: row[:quantidadePareos],
                penetrometro_vf: row[:PenetrometroVF],
                penetrometro_pr: row[:PenetrometroPR],
                cerca_movel: row[:CercaMovel],
                cerca_model_descricao: row[:CercaMovelDescricao],
                pareo_numero: row[:pareoNumero],
                chamada_do_pareo: row[:infoChamada],
                titulo: row[:titulo],
                numero_geral: row[:numeroGeral]
              }
            end
          end

          result[@numero - 1]
        end

        def get_forfaits
          forfaits = []
          
          @animais.each do |animal|
            forfaits << animal.numero if animal.forfait?
          end

          forfaits.join(' - ')
        end

        def get_rateio_especiais
          self.class.get_rateio_especiais @id_reuniao
        end

        def self.get_rateio_especiais id_reuniao
          result = {}

          Jcb::Sisapo::DB.do do |client| 
            result_db = client.execute(%{
              EXEC [dbo].[pTvEspeciaisResultado]
              @idReuniao = #{id_reuniao}
            })
            
            result_db.each(:symbolize_keys => true)
            result_db.each do |especial|
              result[especial[:Descricao].downcase.gsub(/[\s\-]/, '_')] = {
                tipo: especial[:TipApo],
                descricao: especial[:Descricao], 
                formula: especial[:Formula],
                acertos: especial[:Acertos],
                rateio: Util.convert_rateio_to_view(especial[:Rateio].to_f),
                soma_rateios: Util.convert_rateio_to_view(especial[:SomaRateios]),
                a_distribuir: Util.convert_number_to_view(especial[:ADistribuirDia].to_f),
                bonificacao: Util.convert_number_to_view(especial[:Bonificacao].to_f),
                bilhetes: especial[:Bilhetes],
                combinacoes: especial[:Combinacoes]
              }
            end
          end

          result
        end

        def get_forfaits_texts
          retorno = []

          @animais.each do |animal|
            retorno << "#{animal.numero} #{animal.nome} #{animal.medicacao}" if animal.forfait?
          end

          retorno.join(' - ')
        end
    end
  end
end