module Jcb
  module Sisapo
    class Animal
      attr_accessor :nome, :numero, :parelha, :peso, :peso_relativo, :faixa, :criador, :proprietario, :treinador, :codigo_farda, :url_farda, :joquei, :joquei_peso, :joquei_matricula, :joquei_foto_url, :pai, :mae, :avo_materno, :ferradura, :sexo, :idade, :pelagem, :troca_de_montaria, :rateio_parelha, :diferenca, :colocacao, :medicacao, :ultimas_corridas
      attr_writer   :forfait, :desferrado, :estreante

      def forfait?
        @forfait
      end

      def desferrado?
        @desferrado
      end

      def estreante?
        @estreante
      end

      def self.fetch id_reuniao, numero
        result = self.fetch_db id_reuniao, numero

        result.map do |row|
          self.initialize_from_result(row)
        end
      end

      def faixa?
        !faixa.nil?
      end

      def as_json
        {
          nome: nome,
          numero: numero,
          parelha: parelha,
          faixa: faixa,
          criador: criador,
          proprietario: proprietario,
          treinador: treinador,
          codigo_farda: codigo_farda,
          url_farda: url_farda,
          forfait: forfait?,
          peso: peso,
          peso_relativo: peso_relativo,
          joquei: joquei,
          joquei_peso: joquei_peso,
          joquei_matricula: joquei_matricula,
          joquei_foto_url: joquei_foto_url,
          pai: pai,
          mae: mae,
          avo_materno: avo_materno,
          ferradura: ferradura,
          sexo: sexo,
          idade: idade,
          pelagem: pelagem,
          troca_de_montaria: troca_de_montaria,
          rateio_parelha: rateio_parelha,
          diferenca: diferenca,
          desferrado: desferrado?,
          estreante: estreante?,
          colocacao: colocacao,
          medicacao: medicacao,
          ultimas_corridas: ultimas_corridas,
        }
      end

      private

        def self.fetch_db id_reuniao, numero
          result = nil
        
          Jcb::Sisapo::DB.do do |client|
            result = client.execute(%{
              EXEC [dbo].[pTvPareoAnimais]
              @idReuniao = #{id_reuniao},
              @numeroPareo = #{numero}
            })
            
            result.each(:symbolize_keys => true)
          end
          
          ## corrige os tipos de informações
          result.each do |row|
            row[:ordemFavoritismo] = row[:ordemFavoritismo].to_f
            row[:rateioVencedor] = Util.convert_rateio_to_view(row[:rateioVencedor].to_f)
            row[:rateioParelha] = row[:rateioParelha]
          end

          result
        end

        def self.initialize_from_result row
          animal = self.new

          animal.nome              = row[:nomeAnimal]
          animal.numero            = row[:numero]
          animal.parelha           = row[:parelha]
          animal.faixa             = row[:faixa]
          animal.criador           = row[:nomeCriador]
          animal.proprietario      = row[:nomeProprietario]
          animal.treinador         = row[:nomeTreinador]
          animal.codigo_farda      = row[:codigoFarda]
          animal.url_farda         = "http://#{ENV['HOSTNAME']}/fardas/p#{'%06d' % row[:codigoFarda]}.bmp"
          animal.peso              = row[:PesoAnimal]
          animal.peso_relativo     = row[:pesoAnimalRelacao]
          animal.joquei            = row[:nomeJoquei]
          animal.joquei_peso       = row[:pesoJoquei]
          animal.joquei_matricula  = row[:matriculaJoquei]
          animal.joquei_foto_url   = "http://#{ENV['HOSTNAME']}/joqueis/#{row[:matriculaJoquei].to_s.split('').insert(1, '.').join('')}.jpg"
          animal.pai               = row[:nomePai]
          animal.mae               = row[:nomeMae]
          animal.avo_materno       = row[:nomeAvoMaterno]
          animal.ferradura         = row[:ferrageamentoDescricao]
          animal.sexo              = row[:sexoDescricao]
          animal.idade             = row[:idade]
          animal.pelagem           = row[:pelagemDescricao]
          animal.troca_de_montaria = row[:snTrocaDeMontaria]
          animal.rateio_parelha    = Util.convert_rateio_to_view(row[:rateioParelha])
          animal.diferenca         = row[:diferenca]
          animal.forfait           = row[:foiRetirado]
          animal.desferrado        = row[:snDesferrado] == 1
          animal.estreante         = row[:snEstreante] == 1
          animal.colocacao         = row[:colocacaoComissao]
          animal.medicacao         = row[:medicacaoSiglaPO]
          animal.ultimas_corridas  = format_corridas(row[:infoCorridas])

          animal
        end

        def self.format_corridas corridas
          if corridas.nil?
            nil
          else
            corridas.split('|').map do |corrida|
              colunas = corrida.split(',')
              {
                pista: colunas[0],
                hipodromo: colunas[1],
                tipo_corrida: colunas[2],
                colocacao: colunas[3]
              }
            end
          end
        end
    end
  end
end