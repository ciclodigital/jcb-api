require 'tiny_tds'

module Jcb
  module Sisapo
    module DB
      class << self
        attr_accessor :configuration
        attr_accessor :client
      end

      def self.configure
        self.configuration ||= Configuration.new
        yield(configuration)
      end

      class Configuration
        attr_accessor :username
        attr_accessor :password
        attr_accessor :host
        attr_accessor :database
        attr_accessor :port

        def initialize
          @username = nil
          @password = nil
          @host     = '201.48.25.49' #'192.168.10.98'
          @database = 'SisApo'
          @port     = 1499
        end
      end

      def self.config
        self.configuration ||= Configuration.new
      end

      def self.connect
        @client = create_client if self.closed?

        return @client.active?
      end

      def self.create_client
        TinyTds::Client.new username: @configuration.username, password: @configuration.password, host: @configuration.host, database: @configuration.database, port: @configuration.port
      end

      def self.closed?
        @client.nil? || @client.closed? || @client.dead?
      end

      def self.close
        @client.close unless self.closed?
      end

      def self.do &lam
        unless self.connect
          raise "Não foi possível se connectar ao servidor"
        end

        lam.call(@client)
        self.close
      end
    end
  end
end
