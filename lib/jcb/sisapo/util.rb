module Jcb
  module Sisapo
    module Util
      def self.normalize_number_to_real num
        num.to_s.gsub(/\./, ',').reverse.gsub(/(\d{3})(?=\d)/, '\\1.').reverse
      end

      def self.normalize_decimal num, qt=2
        "%.#{qt}f" % num
      end

      def self.convert_number_to_view num
        unless num.nil?
          num = normalize_decimal(num)
          num = normalize_number_to_real(num)
        else
          num
        end
      end

      def self.convert_rateio_to_view num
        unless num.nil?
          num = normalize_decimal(num, 1)
          normalize_number_to_real(num)
        else
          "0,0"
        end
      end
    end
  end
end