module Jcb
  module Sisapo
    module Mga
      ##
      # Deve retornar o MGA de todos os meses do ano.
      #
      # int year Recebe o ano que deseja pesquisar
      # symb canal
      #     :todos
      #     :internet
      #     :agencias
      #     :teleturfe
      #
      def self.fetchCanal options = {}
        result = nil

        o = {
          year: Time.now.year,
          canal: :todos
        }.merge(options)

        Jcb::Sisapo::DB.do do |client|
          result = client.execute(%{
            EXEC [dbo].[pMidiasMGAPorAno]
            @ano = #{o[:year]}
            #{", @canalVenda = #{o[:canal]}" if o[:canal] != :todos }
          })
          
          result.each(:symbolize_keys => true)
        end

        pMidiasMGAPorAnoNormalize(result)
      end

      def self.fetchGrupo options = {}
        result = nil

        o = {
          year: Time.now.year,
          numero: 0,
          canal: :todos
        }.merge(options)

        Jcb::Sisapo::DB.do do |client|
          result = client.execute(%{
            EXEC [dbo].[pMidiasMGAPorGrupo]
            @ano = #{o[:year]}
            #{", @numeroGrupo = #{o[:numero]}" if o[:numero] > 0}
            #{", @canalVenda = #{o[:canal]}" if o[:canal] != :todos }
          })
          
          result.each(:symbolize_keys => true)
        end

        grupo = pMidiasMGAPorGrupoNormalize(result.first)

        if grupo[:mga] == 0.0 && grupo[:numero] > 1
          self.fetchGrupo(o.merge({ numero: grupo[:numero] - 1 }))
        else
          o.merge(grupo)
        end
      end

      def self.fetchGruposPorAno ano = Time.now.year
        result = nil

        Jcb::Sisapo::DB.do do |client|
          result = client.execute(%{
            EXEC [dbo].[pMidiasGruposPorAno] @ano = #{ano}
          })
          
          result.each(:symbolize_keys => true)
        end

        pMidiasGruposPorAnoNormalize(result)
      end

      def self.fetchMgaPorIntervalo inicial=Time.now, final=Time.now + 7 * 24 * 60 * 60, canal=:todos
        result = nil

        dInicial = inicial.strftime('%Y/%m/%d')
        dFinal   = final.strftime('%Y/%m/%d')

        Jcb::Sisapo::DB.do do |client|
          result = client.execute(%{
            EXEC [dbo].[pMidiasMGAPorIntervaloDeDatas]
            @diaInicial = '#{dInicial}',
            @diaFinal = '#{dFinal}'
            #{", @canalVenda = #{canal}" if canal != :todos }
          })
          
          result.each(:symbolize_keys => true)
        end

        pMidiasMGAPorIntervaloDeDatasNormalize(result)
      end

      private
        def self.pMidiasMGAPorAnoNormalize result
          return nil if result.nil?
          
          result.map do |i|
            {
              mes: i[:Mes],
              mga: i[:MGAMes].to_f
            }
          end
        end

        def self.pMidiasMGAPorGrupoNormalize result
          {
            numero: result[:numeroGrupo].to_i,
            descricao: result[:descricaoReunioes],
            mga: result[:MGATotal].to_f
          }
        end

        def self.pMidiasGruposPorAnoNormalize result
          result.map do |i|
            {
              dia_inicial: i[:diaInicial].strftime('%d-%m-%Y'),
              dia_final: i[:diaFinal].strftime('%d-%m-%Y'),
              descricao: i[:descricaoReunioes]
            }
          end
        end

        def self.pMidiasMGAPorIntervaloDeDatasNormalize result
          result.first[:MGATotal].to_f
        end
    end
  end
end