module Jcb
  module Sisapo
    class Reuniao
      attr_accessor :id_reuniao
      attr_accessor :reuniao
      attr_accessor :estado
      attr_accessor :hipodromo
      attr_accessor :dia
      attr_accessor :quantidade_pareos
      attr_accessor :penetrometro_vf
      attr_accessor :penetrometro_pr
      attr_accessor :cerca_movel
      attr_accessor :cerca_movel_descricao
      attr_accessor :mga

      def initialize options={}
        @id_reuniao = options[:id_reuniao]
        @reuniao = options[:reuniao]
        @estado = options[:estado]
        @hipodromo = options[:hipodromo]
        @dia = options[:dia]
        @quantidade_pareos = options[:quantidade_pareos]
        @penetrometro_vf = options[:penetrometro_vf]
        @penetrometro_pr = options[:penetrometro_pr]
        @cerca_movel = options[:cerca_movel]
        @cerca_movel_descricao = options[:cerca_movel_descricao]
        @mga = Util.normalize_number_to_real(options[:mga].to_f) || "0,0"
      end

      ##
      # {"idReuniao"=>42224038, "pareoNumero"=>1, "fechado"=>true, "pista"=>"GM", "distancia"=>"1000", "numeroGeral"=>190, "mensagemRA"=>""}
      def self.fetch day=Time.now, estado=nil
        result = nil
        
        Jcb::Sisapo::DB.do do |client|
          result = client.execute(%{
            EXEC  pTvReunioesDoDia @dia = "#{day.strftime('%Y/%m/%d')}"
          })
          
          ##
          # {:idReuniao=>42225041, :Reuniao=>41, :Hipodromo=>"RJ", :HipodromoDescricao=>"Gávea", :Dia=>2015-08-09 00:00:00 -0300, :quantidadePareos=>12, :PenetrometroVF=>#<BigDecimal:14711d8,'0.38E1',18(18)>, :PenetrometroPR=>#<BigDecimal:14710e8,'0.49E1',18(18)>, :CercaMovel=>#<BigDecimal:1470b48,'0.9E1',9(36)>, :CercaMovelDescricao=>""}
          result.each(:symbolize_keys => true)
        end
        
        reunioes = self.convert_to_reunioes(result)

        if estado.nil?
          reunioes
        else
          reuniao = nil
          
          reunioes.each do |reu|
            reuniao = reu if reu.estado == estado
          end

          reuniao
        end
      end

      def self.convert_to_reunioes rows
        collection = []

        rows.each do |row|
          collection << Reuniao.new({
                       id_reuniao: row[:idReuniao],
                          reuniao: row[:Reuniao],
                           estado: row[:Hipodromo],
                        hipodromo: row[:HipodromoDescricao],
                              dia: row[:Dia].strftime('%d/%m/%Y'),
                quantidade_pareos: row[:quantidadePareos],
                  penetrometro_vf: row[:PenetrometroVF],
                  penetrometro_pr: row[:PenetrometroPR],
                      cerca_movel: row[:CercaMovel],
            cerca_movel_descricao: row[:CercaMovelDescricao],
                              mga: row[:MGA],
          })
        end

        collection
      end
    end
  end
end