require 'json'
require 'byebug'

module Jcb
  module Sisapo
    require 'jcb/sisapo/util'
    require 'jcb/sisapo/db'
    require 'jcb/sisapo/parser'
    require 'jcb/sisapo/reuniao'
    require 'jcb/sisapo/pareo'
    require 'jcb/sisapo/resultado'
    require 'jcb/sisapo/animal'
    require 'jcb/sisapo/mga'
  end
end