require 'jcb'

describe Jcb::Sisapo::Parser do
  subject { Jcb::Sisapo::Parser }

  before(:all) do
    Jcb::Sisapo::DB.configure do |config|
      config.username = 'TvTurfe'
      config.password = 'Jockey123'
    end
  end

  after(:all) do
    Jcb::Sisapo::DB.configure do |config|
      config.username = nil
      config.password = nil
    end
  end

  context ".parse" do
    let(:ra) { "RA080815|142528|RJ|Gávea|38|1|GM^1000|101|FINAL|199##5#167#85441#*939##9#501#110811#*55##3#59#111981#*604##8#328#115541#*248##6#409#109211#*464##7#389#115551#*21##2#22#65101#*107##4#131#44131#*16##1#15#216021#^|72930*14430*141870*26710*46730*11150*250310*63450*264660|07-09#22*03-07#52*03-09#68*07-08#116*08-09#143*03-08#159*01-07#208*01-09#222*05-09#268*05-07#288*01-08#309*03-04#318*01-03#329*03-05#459*02-09#496*06-07#657|09-07#33*07-09#63*03-07#99*09-03#124*03-09#128*07-03#132*09-08#145*08-09#158*03-08#227*08-03#233*09-01#254*07-08#318*07-05#425*08-07#426*03-01#431*01-07#479|09-07-03#106*07-09-03#159*09-07-08#177*09-03-07#181*07-09-08#231*03-07-09#248*07-03-09#271*03-09-07#302*09-07-01#335*09-07-05#389*09-03-08#420*07-09-01#435*09-08-07#498*07-09-05#557*03-07-08#592*07-03-08#601|09-07-03-08#454*09-07-08-03#525*07-09-03-08#566*09-07-03-01#624*09-03-07-08#642*07-09-08-03#642*09-07-03-05#703*09-03-08-07#900*07-03-09-08#908*09-07-01-03#909*09-07-08-01#909*07-09-03-01#961*09-07-05-03#979*03-09-07-08#994*09-07-01-08#1012*09-03-07-01#1042|15186610^13597410^10619650#0#0*892240#0#0*511100#0#0*263960#0#0*495540#0#0*814920#0#0|Pick 7$Pick 7$Pick7$Pck7#1127800#0*^Betting 5$Bett 5$Bett5$Btt5#217600#3300000*^Super Betting$SupBETT$SpBet$SBet#243800#3000000*|" }
    
    it "Must parse Mensagem RA" do
      expect(subject.parse(:ra, ra)).to be_a Hash
    end

    it "Must include hipodromo" do
      parsed = subject.parse(:ra, ra)

      expect(parsed).to include(:hipodromo)
      expect(parsed[:hipodromo]).to eq('Gávea')
    end

    it "Must include estado" do
      parsed = subject.parse(:ra, ra)

      expect(parsed).to include(:estado)
      expect(parsed[:estado]).to eq('RJ')
    end

    it "Must include reuniao" do
      parsed = subject.parse(:ra, ra)

      expect(parsed).to include(:reuniao)
      expect(parsed[:reuniao]).to eq(38)
    end

    it "Must include páreo" do
      parsed = subject.parse(:ra, ra)

      expect(parsed).to include(:pareo)
      expect(parsed[:pareo]).to eq(1)
    end

    it "Must include pareo_tipo" do
      parsed = subject.parse(:ra, ra)

      expect(parsed).to include(:pareo_tipo)
      expect(parsed[:pareo_tipo]).to eq('GM')
    end

    it "Must include pareo_distancia" do
      parsed = subject.parse(:ra, ra)

      expect(parsed).to include(:pareo_distancia)
      expect(parsed[:pareo_distancia]).to eq(1000)
    end

    it "Must include status do páreo" do
      parsed = subject.parse(:ra, ra)

      expect(parsed).to include(:pareo_final)
      expect(parsed).to include(:pareo_alinhamento)
      expect(parsed).to include(:pareo_placar)
      expect(parsed[:pareo_final]).to be_truthy
      expect(parsed[:pareo_alinhamento]).to be_falsey
      expect(parsed[:pareo_placar]).to be_truthy
    end

    it "Must include pareo_prazo" do
      parsed = subject.parse(:ra, ra)

      expect(parsed[:pareo_prazo]).to eq('FINAL')
    end

    it "Must include vencedor" do
      parsed = subject.parse(:ra, ra)

      expect(parsed[:vencedor].count).to eq(9)
    end

    it "Must include place" do
      parsed = subject.parse(:ra, ra)

      expect(parsed[:place].count).to eq(9)
      expect(parsed[:place][0][:apostado]).to eq(729.3)
      expect(parsed[:place][1][:apostado]).to eq(144.3)
      expect(parsed[:place][2][:apostado]).to eq(1418.7)
      expect(parsed[:place][3][:apostado]).to eq(267.1)
      expect(parsed[:place][4][:apostado]).to eq(467.3)
      expect(parsed[:place][5][:apostado]).to eq(111.5)
      expect(parsed[:place][6][:apostado]).to eq(2503.1)
      expect(parsed[:place][7][:apostado]).to eq(634.5)
      expect(parsed[:place][8][:apostado]).to eq(2646.6)
    end

    it "Must include list of duplas" do
      parsed = subject.parse(:ra, ra)

      expect(parsed[:dupla].count).to eq(16)
      expect(parsed[:dupla][0][:combinacao]).to eq('07-09')
      expect(parsed[:dupla][1][:combinacao]).to eq('03-07')
      expect(parsed[:dupla][2][:combinacao]).to eq('03-09')
      expect(parsed[:dupla][3][:combinacao]).to eq('07-08')
      expect(parsed[:dupla][4][:combinacao]).to eq('08-09')
      expect(parsed[:dupla][5][:combinacao]).to eq('03-08')
      expect(parsed[:dupla][6][:combinacao]).to eq('01-07')
      expect(parsed[:dupla][7][:combinacao]).to eq('01-09')
      expect(parsed[:dupla][8][:combinacao]).to eq('05-09')
      expect(parsed[:dupla][9][:combinacao]).to eq('05-07')
      expect(parsed[:dupla][10][:combinacao]).to eq('01-08')
      expect(parsed[:dupla][11][:combinacao]).to eq('03-04')
      expect(parsed[:dupla][12][:combinacao]).to eq('01-03')
      expect(parsed[:dupla][13][:combinacao]).to eq('03-05')
      expect(parsed[:dupla][14][:combinacao]).to eq('02-09')
      expect(parsed[:dupla][15][:combinacao]).to eq('06-07')
      expect(parsed[:dupla][0][:rateio]).to eq(2.2)
      expect(parsed[:dupla][1][:rateio]).to eq(5.2)
      expect(parsed[:dupla][2][:rateio]).to eq(6.8)
      expect(parsed[:dupla][3][:rateio]).to eq(11.6)
      expect(parsed[:dupla][4][:rateio]).to eq(14.3)
      expect(parsed[:dupla][5][:rateio]).to eq(15.9)
      expect(parsed[:dupla][6][:rateio]).to eq(20.8)
      expect(parsed[:dupla][7][:rateio]).to eq(22.2)
      expect(parsed[:dupla][8][:rateio]).to eq(26.8)
      expect(parsed[:dupla][9][:rateio]).to eq(28.8)
      expect(parsed[:dupla][10][:rateio]).to eq(30.9)
      expect(parsed[:dupla][11][:rateio]).to eq(31.8)
      expect(parsed[:dupla][12][:rateio]).to eq(32.9)
      expect(parsed[:dupla][13][:rateio]).to eq(45.9)
      expect(parsed[:dupla][14][:rateio]).to eq(49.6)
      expect(parsed[:dupla][15][:rateio]).to eq(65.7)
    end

    it "Must include list of exatas" do
      parsed = subject.parse(:ra, ra)

      expect(parsed[:exata].count).to eq(16)
      expect(parsed[:exata][0][:combinacao]).to eq('09-07')
      expect(parsed[:exata][1][:combinacao]).to eq('07-09')
      expect(parsed[:exata][2][:combinacao]).to eq('03-07')
      expect(parsed[:exata][3][:combinacao]).to eq('09-03')
      expect(parsed[:exata][4][:combinacao]).to eq('03-09')
      expect(parsed[:exata][5][:combinacao]).to eq('07-03')
      expect(parsed[:exata][6][:combinacao]).to eq('09-08')
      expect(parsed[:exata][7][:combinacao]).to eq('08-09')
      expect(parsed[:exata][8][:combinacao]).to eq('03-08')
      expect(parsed[:exata][9][:combinacao]).to eq('08-03')
      expect(parsed[:exata][10][:combinacao]).to eq('09-01')
      expect(parsed[:exata][11][:combinacao]).to eq('07-08')
      expect(parsed[:exata][12][:combinacao]).to eq('07-05')
      expect(parsed[:exata][13][:combinacao]).to eq('08-07')
      expect(parsed[:exata][14][:combinacao]).to eq('03-01')
      expect(parsed[:exata][15][:combinacao]).to eq('01-07')
      expect(parsed[:exata][0][:rateio]).to eq(3.3)
      expect(parsed[:exata][1][:rateio]).to eq(6.3)
      expect(parsed[:exata][2][:rateio]).to eq(9.9)
      expect(parsed[:exata][3][:rateio]).to eq(12.4)
      expect(parsed[:exata][4][:rateio]).to eq(12.8)
      expect(parsed[:exata][5][:rateio]).to eq(13.2)
      expect(parsed[:exata][6][:rateio]).to eq(14.5)
      expect(parsed[:exata][7][:rateio]).to eq(15.8)
      expect(parsed[:exata][8][:rateio]).to eq(22.7)
      expect(parsed[:exata][9][:rateio]).to eq(23.3)
      expect(parsed[:exata][10][:rateio]).to eq(25.4)
      expect(parsed[:exata][11][:rateio]).to eq(31.8)
      expect(parsed[:exata][12][:rateio]).to eq(42.5)
      expect(parsed[:exata][13][:rateio]).to eq(42.6)
      expect(parsed[:exata][14][:rateio]).to eq(43.1)
      expect(parsed[:exata][15][:rateio]).to eq(47.9)
    end

    it "Must be include totais" do
      parsed = subject.parse(:ra, ra)

      expect(parsed[:total_reuniao]).to eq(151866.10)
      expect(parsed[:total_pareo]).to eq(135974.10)
      expect(parsed[:total_modalidades].size).to eq(6)
    end
  end

  context ".parse_ra_vencedor" do
    it "Must parse animal" do
      vencedor = subject.parse_ra_vencedor(1, '199##5#167#85441#')

      expect(vencedor[:numero]).to eq(1)
      expect(vencedor[:rateio]).to eq(19.9)
      expect(vencedor[:parelha]).to be_empty
      expect(vencedor[:favoritismo]).to eq(5)
      expect(vencedor[:rateio_abertura]).to eq(16.7)
      expect(vencedor[:codigo_farda]).to eq("85441")
    end

    it "Must parse animal with rateio -1" do
      vencedor = subject.parse_ra_vencedor(1, '-10##5#167#85441#')

      expect(vencedor[:numero]).to eq(1)
      expect(vencedor[:rateio]).to eq(-1.0)
      expect(vencedor[:parelha]).to be_empty
      expect(vencedor[:favoritismo]).to eq(5)
      expect(vencedor[:rateio_abertura]).to eq(16.7)
      expect(vencedor[:codigo_farda]).to eq("85441")
    end
  end

  context ".parse_ra_place" do
    it "Must parse place" do
      place = subject.parse_ra_place(1, '72930')

      expect(place[:numero]  ).to eq(1)
      expect(place[:apostado]).to eq(729.30)
    end
  end

  context ".parse_ra_combinada" do
    it "Must parse dupla" do
      place = subject.parse_ra_combinada('07-09#22')

      expect(place[:combinacao]  ).to eq('07-09')
      expect(place[:rateio]).to eq(2.2)
    end

    it "Must parse exata" do
      place = subject.parse_ra_combinada('09-07#33')

      expect(place[:combinacao]  ).to eq('09-07')
      expect(place[:rateio]).to eq(3.3)
    end

    it "Must parse trifera" do
      place = subject.parse_ra_combinada('09-07-03#106')

      expect(place[:combinacao]  ).to eq('09-07-03')
      expect(place[:rateio]).to eq(10.6)
    end

    it "Must parse quadrifeta" do
      place = subject.parse_ra_combinada('09-07-03-08#454')

      expect(place[:combinacao]  ).to eq('09-07-03-08')
      expect(place[:rateio]).to eq(45.4)
    end
  end

  context ".parse_ra_totais" do
    it "Must parse totais" do
      parsed = subject.parse_ra_totais('15186610^13597410^10619650#0#0*892240#0#0*511100#0#0*263960#0#0*495540#0#0*814920#0#0')

      expect(parsed[:total_reuniao]).to eq(151866.10)
      expect(parsed[:total_pareo]).to eq(135974.10)
      expect(parsed[:total_modalidades].size).to eq(6)
    end
  end

  context ".parse_ra_totais_modalidades" do
    it "Must parse totais das modalidades" do
      parsed = subject.parse_ra_totais_modalidades('10619650#0#0*892240#0#0*511100#0#0*263960#0#0*495540#0#0*814920#0#0')

      expect(parsed.size).to eq(6)
      expect(parsed).to include(:vencedor)
      expect(parsed).to include(:place)
      expect(parsed).to include(:dupla)
      expect(parsed).to include(:exata)
      expect(parsed).to include(:trifeta)
      expect(parsed).to include(:quadrifeta)

      expect(parsed[:vencedor][:total]).to eq(106196.50)
      expect(parsed[:vencedor][:acrescimo]).to eq(0.0)
      expect(parsed[:vencedor][:garantia]).to eq(0.0)
    end
  end

  context ".parse_ra_especiais" do
    it "Must parse lideres" do
      parsed = subject.parse_ra_especiais('Pick 7$Pick 7$Pick7$Pck7#1127800#0*^Betting 5$Bett 5$Bett5$Btt5#217600#3300000*^Super Betting$SupBETT$SpBet$SBet#243800#3000000*')


      expect(parsed.size).to eq(3)
      expect(parsed["pick_7"][:labels].size).to eq(4)
      expect(parsed["pick_7"][:total]).to eq(11278.0)
      expect(parsed["pick_7"][:bonificacao]).to eq(0.0)
    end
  end
end