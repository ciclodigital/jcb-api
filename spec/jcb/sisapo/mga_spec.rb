require 'jcb'

describe Jcb::Sisapo::Mga do
  subject { Jcb::Sisapo::Mga }

  before(:all) do
    Jcb::Sisapo::DB.configure do |config|
      config.username = "midiassociais"
      config.password = "jcb123"
      config.host = '192.168.0.179'
    end
  end

  after(:all) do
    Jcb::Sisapo::DB.configure do |config|
      config.username = nil
      config.password = nil
    end
  end

  context ".porGrupo" do
    it "Deve retornar a soma de todos os canais" do
      puts subject.fetchCanal
    end
  end

  context ".fetchGruposPorAno" do
    it "Deve retornar os grupos de reuniao" do
      puts subject.fetchGruposPorAno
    end
  end

  context ".fetchMgaPorIntervalo" do
    it "Deve retornar o mg do intervalo" do
      puts subject.fetchMgaPorIntervalo Time.now - 7 * 24 * 60 * 60, Time.now
    end
  end

end