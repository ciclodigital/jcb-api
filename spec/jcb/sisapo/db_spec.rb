require 'jcb'

describe Jcb::Sisapo::DB do
  subject { Jcb::Sisapo::DB }
  
  context "#config" do
    it "Must unknow a credentials" do
      expect(subject.config.username).to be_nil
      expect(subject.config.password).to be_nil
      expect(subject.config.database).to eq('SisApo')
      expect(subject.config.host    ).to eq("sisapo.serveftp.net")
      expect(subject.config.port    ).to eq(1499)
    end

    it "Must configure a credentials" do
      username = 'username'
      password = 'password'

      subject.configure do |config|
        config.username = username
        config.password = password
      end

      expect(subject.config.username).to eq(username)
      expect(subject.config.password).to eq(password)
    end
  end

  context "connect" do
    it "Must not connect to db" do
      expect{ subject.connect }.to raise_error(TinyTds::Error)
    end

    it "Must connect to db" do
      subject.configure do |config|
        config.username = 'TvTurfe'
        config.password = 'Jockey123'
      end

      expect(subject.connect).to be_truthy
    end
  end

  context "#do" do
    it "Must open and close a client" do
      clt = nil
      subject.do do |client|
        clt = client
        expect(client).to be_active
      end 

      expect(clt).to be_closed
    end
  end
end