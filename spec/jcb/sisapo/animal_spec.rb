require 'jcb'
require 'byebug'

describe Jcb::Sisapo::Animal do
  subject { Jcb::Sisapo::Animal }
  
  before(:all) do
    Jcb::Sisapo::DB.configure do |config|
      config.username = 'TvTurfe'
      config.password = 'Jockey123'
    end
  end

  let(:animais_com_resultado) {
    [
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>1, :faixa=>0, :parelha=>0, :nomeAnimal=>"VIROU UMA ONÇA", :nomeProprietario=>"Stud Acqua Azul", :nomeCriador=>"Haras Ponta Porã", :nomeTreinador=>"C.Ricardo", :nomePai=>"SILENT NAME", :nomeMae=>"ONÇA LIGEIRA", :nomeAvoMaterno=>"FROM CARSON CITY", :nomeJoquei=>"F.Chaves", :PesoAnimal=>"465", :pesoJoquei=>"57", :ferraduraTroca=>"A", :idCompetidor=>1010, :sexo=>"F", :idade=>5, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"=", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Castanha", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>8, :colocacaoComissaoDescricao=>"8º", :rateioVencedor=>19.9, :rateioParelha=>0.0, :ordemFavoritismo=>5.0, :diferenca=>"5 1/4", :codigoFarda=>"85441", :snDesferrado=>0, :snEstreante=>0},
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>2, :faixa=>0, :parelha=>0, :nomeAnimal=>"BRINE", :nomeProprietario=>"Stud Joaquin e Emiliano", :nomeCriador=>"Haras Santa Maria de Araras", :nomeTreinador=>"J.F.Reis", :nomePai=>"NORTHERN AFLEET", :nomeMae=>"OTTAGE", :nomeAvoMaterno=>"BRIGHT AGAIN", :nomeJoquei=>"A.M.Silva", :PesoAnimal=>"441", :pesoJoquei=>"57", :ferraduraTroca=>"A", :idCompetidor=>1020, :sexo=>"F", :idade=>5, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"+8", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Castanha", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>7, :colocacaoComissaoDescricao=>"7º", :rateioVencedor=>93.9, :rateioParelha=>0.0, :ordemFavoritismo=>9.0, :diferenca=>"5", :codigoFarda=>"110811", :snDesferrado=>0, :snEstreante=>0},
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>3, :faixa=>0, :parelha=>0, :nomeAnimal=>"SAVAGE GIRL", :nomeProprietario=>"Stud Gata da Serra", :nomeCriador=>"Haras Ereporã", :nomeTreinador=>"C.Ricardo", :nomePai=>"IMPRESSION", :nomeMae=>"ENGLISH GIRL", :nomeAvoMaterno=>"EXILE KING", :nomeJoquei=>"A.Correia", :PesoAnimal=>"438", :pesoJoquei=>"55", :ferraduraTroca=>"A", :idCompetidor=>1030, :sexo=>"F", :idade=>7, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"-3", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Castanha", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>1, :colocacaoComissaoDescricao=>"1º", :rateioVencedor=>5.5, :rateioParelha=>0.0, :ordemFavoritismo=>3.0, :diferenca=>"2", :codigoFarda=>"111981", :snDesferrado=>0, :snEstreante=>0},
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>4, :faixa=>0, :parelha=>0, :nomeAnimal=>"SAMPDORIA", :nomeProprietario=>"Stud São Pedro Velho", :nomeCriador=>"Haras Old Friends Ltda", :nomeTreinador=>"J.James", :nomePai=>"VETTORI", :nomeMae=>"KIND GIRL", :nomeAvoMaterno=>"ROYAL ACADEMY", :nomeJoquei=>"W.Freitas", :PesoAnimal=>"426", :pesoJoquei=>"57", :ferraduraTroca=>"A", :idCompetidor=>1040, :sexo=>"F", :idade=>5, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"-3", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Castanha", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>3, :colocacaoComissaoDescricao=>"3º", :rateioVencedor=>60.4, :rateioParelha=>0.0, :ordemFavoritismo=>8.0, :diferenca=>"2 1/2", :codigoFarda=>"115541", :snDesferrado=>0, :snEstreante=>0},
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>5, :faixa=>0, :parelha=>0, :nomeAnimal=>"MILLEMIGLIA", :nomeProprietario=>"Stud Valbom", :nomeCriador=>"Fazenda Mondesir", :nomeTreinador=>"M.Teixeira", :nomePai=>"PUBLIC PURSE", :nomeMae=>"ETOILE BLANC", :nomeAvoMaterno=>"NEDAWI", :nomeJoquei=>"E.Ferreira Filho", :PesoAnimal=>"415", :pesoJoquei=>"57", :ferraduraTroca=>"A", :idCompetidor=>1050, :sexo=>"F", :idade=>6, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"+2", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Castanha", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>5, :colocacaoComissaoDescricao=>"5º", :rateioVencedor=>24.8, :rateioParelha=>0.0, :ordemFavoritismo=>6.0, :diferenca=>"4", :codigoFarda=>"109211", :snDesferrado=>0, :snEstreante=>0},
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>6, :faixa=>0, :parelha=>0, :nomeAnimal=>"COUCH TRIP", :nomeProprietario=>"Stud Epifanio", :nomeCriador=>"Haras Santa Maria de Araras", :nomeTreinador=>"J.S.Guerra", :nomePai=>"PUT IT BACK", :nomeMae=>"OGGETTIVITÀ", :nomeAvoMaterno=>"JULES", :nomeJoquei=>"B.Pinheiro", :PesoAnimal=>"455", :pesoJoquei=>"58", :ferraduraTroca=>"A", :idCompetidor=>1060, :sexo=>"F", :idade=>4, :pelagem=>"A", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"-3", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Alazã", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>9, :colocacaoComissaoDescricao=>"9º", :rateioVencedor=>46.4, :rateioParelha=>0.0, :ordemFavoritismo=>7.0, :diferenca=>"5 1/2", :codigoFarda=>"115551", :snDesferrado=>0, :snEstreante=>0},
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>7, :faixa=>0, :parelha=>0, :nomeAnimal=>"VALE FORTUNA", :nomeProprietario=>"Stud Winchester 45", :nomeCriador=>"Haras Ponta Porã", :nomeTreinador=>"O.Ulloa Neto", :nomePai=>"SILENT NAME", :nomeMae=>"FOREFORTUNE", :nomeAvoMaterno=>"MINING", :nomeJoquei=>"M.S.Machado", :PesoAnimal=>"472", :pesoJoquei=>"58", :ferraduraTroca=>"A", :idCompetidor=>1070, :sexo=>"F", :idade=>5, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"=", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Castanha", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>6, :colocacaoComissaoDescricao=>"6º", :rateioVencedor=>2.1, :rateioParelha=>0.0, :ordemFavoritismo=>2.0, :diferenca=>"4 1/2", :codigoFarda=>"65101", :snDesferrado=>0, :snEstreante=>0},
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>8, :faixa=>0, :parelha=>0, :nomeAnimal=>"INDEPENDENCE", :nomeProprietario=>"Haras Praça XV", :nomeCriador=>"Haras Santa Camila", :nomeTreinador=>"E.Ricardo", :nomePai=>"ELUSIVE QUALITY", :nomeMae=>"RUGA", :nomeAvoMaterno=>"ROI NORMAND", :nomeJoquei=>"V.Borges", :PesoAnimal=>"431", :pesoJoquei=>"57", :ferraduraTroca=>"A", :idCompetidor=>1080, :sexo=>"F", :idade=>5, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"+7", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Castanha", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>4, :colocacaoComissaoDescricao=>"4º", :rateioVencedor=>10.7, :rateioParelha=>0.0, :ordemFavoritismo=>4.0, :diferenca=>"2 3/4", :codigoFarda=>"44131", :snDesferrado=>0, :snEstreante=>0},
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>9, :faixa=>0, :parelha=>0, :nomeAnimal=>"VELHA NAMORADA", :nomeProprietario=>"Fabiano Neiva Fernandes", :nomeCriador=>"Haras Ponta Porã", :nomeTreinador=>"L.Linhares", :nomePai=>"BLADE PROSPECTOR", :nomeMae=>"NAMORADA NOVA", :nomeAvoMaterno=>"PURPLE MOUNTAIN", :nomeJoquei=>"L.Henrique Ap3", :PesoAnimal=>"Est", :pesoJoquei=>"58/5", :ferraduraTroca=>"A", :idCompetidor=>1090, :sexo=>"F", :idade=>5, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"477", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Castanha", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>2, :colocacaoComissaoDescricao=>"2º", :rateioVencedor=>1.6, :rateioParelha=>0.0, :ordemFavoritismo=>1.0, :diferenca=>"2", :codigoFarda=>"216021", :snDesferrado=>0, :snEstreante=>1}
    ]
  }

  let(:animais_sem_resultado) {
    [
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>1, :faixa=>0, :parelha=>0, :nomeAnimal=>"WALDO SUPER", :nomeProprietario=>"Francisco Raymundo Neto", :nomeCriador=>"Haras Chiodelli", :nomeTreinador=>"G.A.Feijó", :nomePai=>"WAIMEA SUPER", :nomeMae=>"SUMMER LUCK", :nomeAvoMaterno=>"DODGE", :nomeJoquei=>"A.Mendes", :PesoAnimal=>"456", :pesoJoquei=>"58", :ferraduraTroca=>"A", :idCompetidor=>1010, :sexo=>"M", :idade=>4, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"-456", :sexoDescricao=>"Macho", :pelagemDescricao=>"Castanho", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>0, :colocacaoComissaoDescricao=>"", :rateioVencedor=>0.0, :rateioParelha=>0.0, :ordemFavoritismo=>0.0, :diferenca=>"", :codigoFarda=>"113491", :snDesferrado=>0, :snEstreante=>0},
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>2, :faixa=>0, :parelha=>0, :nomeAnimal=>"GARRINHA", :nomeProprietario=>"Stud Golubcik Stables", :nomeCriador=>"Haras Valente", :nomeTreinador=>"O.Loezer", :nomePai=>"BONAPARTISTE", :nomeMae=>"AMPULHETA", :nomeAvoMaterno=>"LORD MARCOS", :nomeJoquei=>"H.Fernandes", :PesoAnimal=>"442", :pesoJoquei=>"58", :ferraduraTroca=>"A", :idCompetidor=>1020, :sexo=>"M", :idade=>4, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"-442", :sexoDescricao=>"Macho", :pelagemDescricao=>"Castanho", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>0, :colocacaoComissaoDescricao=>"", :rateioVencedor=>0.0, :rateioParelha=>0.0, :ordemFavoritismo=>0.0, :diferenca=>"", :codigoFarda=>"116261", :snDesferrado=>0, :snEstreante=>0},
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>3, :faixa=>0, :parelha=>0, :nomeAnimal=>"WAG", :nomeProprietario=>"Coudelaria Bob Mussi", :nomeCriador=>"Haras J.G.", :nomeTreinador=>"L.Linhares", :nomePai=>"ARTAX", :nomeMae=>"QUILL ALADA", :nomeAvoMaterno=>"MENSAGEIRO ALADO", :nomeJoquei=>"L.S.Machado", :PesoAnimal=>"573", :pesoJoquei=>"58", :ferraduraTroca=>"A", :idCompetidor=>1030, :sexo=>"M", :idade=>4, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"-573", :sexoDescricao=>"Macho", :pelagemDescricao=>"Castanho", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>0, :colocacaoComissaoDescricao=>"", :rateioVencedor=>0.0, :rateioParelha=>0.0, :ordemFavoritismo=>0.0, :diferenca=>"", :codigoFarda=>"113561", :snDesferrado=>0, :snEstreante=>0},
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>4, :faixa=>0, :parelha=>0, :nomeAnimal=>"DACUNTO", :nomeProprietario=>"Stud Brializ", :nomeCriador=>"Haras Basano", :nomeTreinador=>"J.A.Silva", :nomePai=>"CRAFTY C.T.", :nomeMae=>"PREMIATRA", :nomeAvoMaterno=>"KNOW HEIGHTS", :nomeJoquei=>"V.Borges", :PesoAnimal=>"436", :pesoJoquei=>"58", :ferraduraTroca=>"A", :idCompetidor=>1040, :sexo=>"M", :idade=>4, :pelagem=>"A", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"-436", :sexoDescricao=>"Macho", :pelagemDescricao=>"Alazão", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>0, :colocacaoComissaoDescricao=>"", :rateioVencedor=>0.0, :rateioParelha=>0.0, :ordemFavoritismo=>0.0, :diferenca=>"", :codigoFarda=>"100701", :snDesferrado=>0, :snEstreante=>0},
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>5, :faixa=>0, :parelha=>0, :nomeAnimal=>"BADMINTON", :nomeProprietario=>"Stud Chreem", :nomeCriador=>"Stud TNT", :nomeTreinador=>"R.Nahid", :nomePai=>"ELUSIVE QUALITY", :nomeMae=>"MAGIC RAFAELA", :nomeAvoMaterno=>"ROYAL ACADEMY", :nomeJoquei=>"B.Pinheiro", :PesoAnimal=>"526", :pesoJoquei=>"58", :ferraduraTroca=>"A", :idCompetidor=>1050, :sexo=>"M", :idade=>4, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"-526", :sexoDescricao=>"Macho", :pelagemDescricao=>"Castanho", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>0, :colocacaoComissaoDescricao=>"", :rateioVencedor=>0.0, :rateioParelha=>0.0, :ordemFavoritismo=>0.0, :diferenca=>"", :codigoFarda=>"58561", :snDesferrado=>0, :snEstreante=>0},
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>6, :faixa=>0, :parelha=>0, :nomeAnimal=>"ENERGIA GLOCK", :nomeProprietario=>"Stud Golden Horses", :nomeCriador=>"Haras Estrela Energia", :nomeTreinador=>"J.Borges", :nomePai=>"AGNES GOLD", :nomeMae=>"ESTRELA BELA", :nomeAvoMaterno=>"CLACKSON", :nomeJoquei=>"V.Gil", :PesoAnimal=>"480", :pesoJoquei=>"58", :ferraduraTroca=>"A", :idCompetidor=>1060, :sexo=>"M", :idade=>4, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"-480", :sexoDescricao=>"Macho", :pelagemDescricao=>"Castanho", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>0, :colocacaoComissaoDescricao=>"", :rateioVencedor=>0.0, :rateioParelha=>0.0, :ordemFavoritismo=>0.0, :diferenca=>"", :codigoFarda=>"99821", :snDesferrado=>0, :snEstreante=>0},
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>7, :faixa=>0, :parelha=>0, :nomeAnimal=>"JOCKER", :nomeProprietario=>"Ubiratan Ximenes", :nomeCriador=>"Haras Santa Camila", :nomeTreinador=>"A.Castillo", :nomePai=>"ELUSIVE QUALITY", :nomeMae=>"AFTER SISTER", :nomeAvoMaterno=>"THIGNON LAFRÉ", :nomeJoquei=>"L.Henrique Ap3", :PesoAnimal=>"512", :pesoJoquei=>"58/5", :ferraduraTroca=>"A", :idCompetidor=>1070, :sexo=>"M", :idade=>4, :pelagem=>"A", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"-512", :sexoDescricao=>"Macho", :pelagemDescricao=>"Alazão", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>0, :colocacaoComissaoDescricao=>"", :rateioVencedor=>0.0, :rateioParelha=>0.0, :ordemFavoritismo=>0.0, :diferenca=>"", :codigoFarda=>"87681", :snDesferrado=>0, :snEstreante=>0},
      {:foiRetirado=>false, :pareoNumero=>1, :numero=>8, :faixa=>0, :parelha=>0, :nomeAnimal=>"CANCIONERO", :nomeProprietario=>"Stud Caio", :nomeCriador=>"Haras Balada", :nomeTreinador=>"D.Lopes", :nomePai=>"MASTRO LORENZO", :nomeMae=>"MINHA QUERÊNCIA", :nomeAvoMaterno=>"CLACKSON", :nomeJoquei=>"R.Costa", :PesoAnimal=>"444", :pesoJoquei=>"58", :ferraduraTroca=>"A", :idCompetidor=>1080, :sexo=>"M", :idade=>4, :pelagem=>"A", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"-444", :sexoDescricao=>"Macho", :pelagemDescricao=>"Alazão", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>0, :colocacaoComissaoDescricao=>"", :rateioVencedor=>0.0, :rateioParelha=>0.0, :ordemFavoritismo=>0.0, :diferenca=>"", :codigoFarda=>"116341", :snDesferrado=>0, :snEstreante=>0}
    ]
  }

  let(:animais_com_resultado_crom_parelha) {
    [
      {:foiRetirado=>false, :pareoNumero=>7, :numero=>1, :faixa=>0, :parelha=>0, :nomeAnimal=>"GOSPEL SINGER", :nomeProprietario=>"Stud Big Feeling", :nomeCriador=>"Haras Santa Rita da Serra", :nomeTreinador=>"J.Borges", :nomePai=>"SILENT TIMES", :nomeMae=>"NEW YORK DIXIE", :nomeAvoMaterno=>"DIXIELAND BAND", :nomeJoquei=>"E.Ferreira Filho", :PesoAnimal=>"Est", :pesoJoquei=>"56", :ferraduraTroca=>"A", :idCompetidor=>7010, :sexo=>"F", :idade=>2, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"402", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Castanha", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>1, :colocacaoComissaoDescricao=>"1º", :rateioVencedor=>"4,4", :rateioParelha=>nil, :ordemFavoritismo=>3.0, :diferenca=>"Pescoço", :codigoFarda=>"99051", :snDesferrado=>0, :snEstreante=>1, :infoCorridas=>"*Inédito em Hip.Oficiais", :medicacaoSiglaPO=>"", :matriculaJoquei=>"20202"},
      {:foiRetirado=>false, :pareoNumero=>7, :numero=>2, :faixa=>0, :parelha=>0, :nomeAnimal=>"GAGNOA", :nomeProprietario=>"Marlene Fernandes Serrador", :nomeCriador=>"Haras Santa Rita da Serra", :nomeTreinador=>"T.Penelas", :nomePai=>"SILENT TIMES", :nomeMae=>"NINA PRETA", :nomeAvoMaterno=>"NEW COLONY", :nomeJoquei=>"M.S.Machado", :PesoAnimal=>"Est", :pesoJoquei=>"56", :ferraduraTroca=>"A", :idCompetidor=>7020, :sexo=>"F", :idade=>2, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"434", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Castanha", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>8, :colocacaoComissaoDescricao=>"8º", :rateioVencedor=>"24,8", :rateioParelha=>nil, :ordemFavoritismo=>7.0, :diferenca=>"23 1/2", :codigoFarda=>"56511", :snDesferrado=>0, :snEstreante=>1, :infoCorridas=>"*Inédito em Hip.Oficiais", :medicacaoSiglaPO=>"", :matriculaJoquei=>"20227"},
      {:foiRetirado=>false, :pareoNumero=>7, :numero=>3, :faixa=>0, :parelha=>0, :nomeAnimal=>"JOLIE MABI", :nomeProprietario=>"Haras Figueira do Lago", :nomeCriador=>"Haras Figueira do Lago", :nomeTreinador=>"D.Guignoni", :nomePai=>"ROCK OF GIBRALTAR", :nomeMae=>"DONATELA BELA", :nomeAvoMaterno=>"TORRENTIAL", :nomeJoquei=>"L.Henrique", :PesoAnimal=>"Est", :pesoJoquei=>"56", :ferraduraTroca=>"A", :idCompetidor=>7030, :sexo=>"F", :idade=>2, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"438", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Castanha", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>2, :colocacaoComissaoDescricao=>"2º", :rateioVencedor=>"2,8", :rateioParelha=>nil, :ordemFavoritismo=>2.0, :diferenca=>"Pescoço", :codigoFarda=>"112501", :snDesferrado=>0, :snEstreante=>1, :infoCorridas=>"*Inédito em Hip.Oficiais", :medicacaoSiglaPO=>"", :matriculaJoquei=>"20240"},
      {:foiRetirado=>false, :pareoNumero=>7, :numero=>4, :faixa=>0, :parelha=>0, :nomeAnimal=>"GARBOSA CRIS", :nomeProprietario=>"Haras Basano", :nomeCriador=>"Haras Basano", :nomeTreinador=>"J.Borges", :nomePai=>"SIPHON", :nomeMae=>"BABY CRISTINA", :nomeAvoMaterno=>"HINTON WELLS", :nomeJoquei=>"H.Fernandes", :PesoAnimal=>"Est", :pesoJoquei=>"56", :ferraduraTroca=>"A", :idCompetidor=>7040, :sexo=>"F", :idade=>2, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"423", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Castanha", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>4, :colocacaoComissaoDescricao=>"4º", :rateioVencedor=>"9,7", :rateioParelha=>nil, :ordemFavoritismo=>6.0, :diferenca=>"10 1/2", :codigoFarda=>"88831", :snDesferrado=>0, :snEstreante=>1, :infoCorridas=>"*Inédito em Hip.Oficiais", :medicacaoSiglaPO=>"", :matriculaJoquei=>"20195"},
      {:foiRetirado=>false, :pareoNumero=>7, :numero=>5, :faixa=>0, :parelha=>0, :nomeAnimal=>"FLOWER PURSE", :nomeProprietario=>"Haras Anderson", :nomeCriador=>"Haras Anderson", :nomeTreinador=>"J.Pessanha", :nomePai=>"PUBLIC PURSE", :nomeMae=>"EXTERMINADORA", :nomeAvoMaterno=>"NEDAWI", :nomeJoquei=>"I.Correa", :PesoAnimal=>"Est", :pesoJoquei=>"56", :ferraduraTroca=>"A", :idCompetidor=>7050, :sexo=>"F", :idade=>2, :pelagem=>"A", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"420", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Alazã", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>7, :colocacaoComissaoDescricao=>"7º", :rateioVencedor=>"2,6", :rateioParelha=>nil, :ordemFavoritismo=>1.0, :diferenca=>"20 1/2", :codigoFarda=>"65971", :snDesferrado=>0, :snEstreante=>1, :infoCorridas=>"*Inédito em Hip.Oficiais", :medicacaoSiglaPO=>"", :matriculaJoquei=>"20129"},
      {:foiRetirado=>false, :pareoNumero=>7, :numero=>5, :faixa=>1, :parelha=>0, :nomeAnimal=>"FLOWER TOUCH", :nomeProprietario=>"Haras Anderson", :nomeCriador=>"Haras Anderson", :nomeTreinador=>"J.Pessanha", :nomePai=>"PUBLIC PURSE", :nomeMae=>"OUT OF TOUCH(USA)", :nomeAvoMaterno=>"ARCH", :nomeJoquei=>"C.Lavor", :PesoAnimal=>"Est", :pesoJoquei=>"56", :ferraduraTroca=>"A", :idCompetidor=>7051, :sexo=>"F", :idade=>2, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"403", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Castanha", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>6, :colocacaoComissaoDescricao=>"6º", :rateioVencedor=>"2,6", :rateioParelha=>nil, :ordemFavoritismo=>1.0, :diferenca=>"20 1/4", :codigoFarda=>"65972", :snDesferrado=>0, :snEstreante=>1, :infoCorridas=>"*Inédito em Hip.Oficiais", :medicacaoSiglaPO=>"", :matriculaJoquei=>"20018"},
      {:foiRetirado=>false, :pareoNumero=>7, :numero=>6, :faixa=>0, :parelha=>0, :nomeAnimal=>"TANTINY", :nomeProprietario=>"Haras São José da Serra", :nomeCriador=>"Haras São José da Serra", :nomeTreinador=>"D.Guignoni", :nomePai=>"TIMEO", :nomeMae=>"TCHELONA", :nomeAvoMaterno=>"NORTHERN AFLEET", :nomeJoquei=>"W.S.Cardoso", :PesoAnimal=>"Est", :pesoJoquei=>"56", :ferraduraTroca=>"A", :idCompetidor=>7060, :sexo=>"F", :idade=>2, :pelagem=>"C", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"440", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Castanha", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>3, :colocacaoComissaoDescricao=>"3º", :rateioVencedor=>"7,7", :rateioParelha=>nil, :ordemFavoritismo=>5.0, :diferenca=>"2 1/2", :codigoFarda=>"50651", :snDesferrado=>0, :snEstreante=>1, :infoCorridas=>"*Inédito em Hip.Oficiais", :medicacaoSiglaPO=>"", :matriculaJoquei=>"20231"},
      {:foiRetirado=>false, :pareoNumero=>7, :numero=>7, :faixa=>0, :parelha=>0, :nomeAnimal=>"JULIA GIRL", :nomeProprietario=>"Stud Best Friends", :nomeCriador=>"Stud TNT / Haras Vale Verde", :nomeTreinador=>"I.C.Souza", :nomePai=>"REDATTORE", :nomeMae=>"MAGIC KELLER", :nomeAvoMaterno=>"ROYAL ACADEMY", :nomeJoquei=>"B.Pinheiro", :PesoAnimal=>"Est", :pesoJoquei=>"56", :ferraduraTroca=>"A", :idCompetidor=>7070, :sexo=>"F", :idade=>2, :pelagem=>"A", :snTrocaDeMontaria=>false, :pesoAnimalRelacao=>"415", :sexoDescricao=>"Fêmea", :pelagemDescricao=>"Alazã", :ferrageamentoDescricao=>"Alumínio", :colocacaoComissao=>5, :colocacaoComissaoDescricao=>"5º", :rateioVencedor=>"6,5", :rateioParelha=>nil, :ordemFavoritismo=>4.0, :diferenca=>"15 1/2", :codigoFarda=>"83801", :snDesferrado=>0, :snEstreante=>1, :infoCorridas=>"*Inédito em Hip.Oficiais", :medicacaoSiglaPO=>"", :matriculaJoquei=>"20232"}
    ]
  }

  it "Deve retornar os animais de um páreo" do
    allow(subject).to receive(:fetch_db).with(42224038, 1) { animais_com_resultado }

    animais = subject.fetch 42224038, 1

    expect(animais.count).to eq(9)
  end

  it "Deve retornar um array de animais de um páreo" do
    allow(subject).to receive(:fetch_db).with(42224038, 1) { animais_com_resultado }

    animais = subject.fetch 42224038, 1
    first   = animais.first

    expect(first).to be_a subject
    expect(first.nome).to eq('VIROU UMA ONÇA')
    expect(first.numero).to eq('1')
    expect(first.faixa).to eq(0)
    expect(first.parelha).to eq(0)
    expect(first.criador).to eq('Haras Ponta Porã')
    expect(first.proprietario).to eq('Stud Acqua Azul')
    expect(first.treinador).to eq('C.Ricardo')
    expect(first.codigo_farda).to eq('85441')
    expect(first.joquei).to eq('F.Chaves')
    expect(first.peso).to eq('465')
    expect(first.joquei_peso).to eq('57')
    expect(first.pai).to eq('SILENT NAME')
    expect(first.mae).to eq('ONÇA LIGEIRA')
    expect(first.avo_materno).to eq('FROM CARSON CITY')
    expect(first.ferradura).to eq('Alumínio')
    expect(first.sexo).to eq('Fêmea')
    expect(first.idade).to eq(5)
    expect(first.pelagem).to eq('Castanha')
    expect(first.troca_de_montaria).to be_falsey
    expect(first.peso_relativo).to eq('=')
    expect(first.rateio_parelha).to eq('0,0')
    expect(first.diferenca).to eq('5 1/4')
    expect(first.colocacao).to eq(8)

    expect(first).not_to be_forfait
    expect(first).not_to be_desferrado
    expect(first).not_to be_estreante
  end

  it "animais_com_resultado_crom_parelha" do
    allow(subject).to receive(:fetch_db).with(42722302, 7) { animais_com_resultado_crom_parelha }

    animais = subject.fetch 42722302, 7

    expect( animais[4].numero ).to eq(5)
    expect( animais[4].faixa? ).to eq(true)
    expect( animais[4].faixa ).to eq(0)
    expect( animais[5].numero ).to eq(5)
    expect( animais[5].faixa? ).to eq(true)
    expect( animais[5].faixa ).to eq(1)
  end

  after(:all) do
    Jcb::Sisapo::DB.configure do |config|
      config.username = nil
      config.password = nil
    end
  end
end