require 'jcb'

describe Jcb::Sisapo::Reuniao do
  subject { Jcb::Sisapo::Reuniao }

  before(:all) do
    Jcb::Sisapo::DB.configure do |config|
      config.username = 'TvTurfe'
      config.password = 'Jockey123'
    end
  end

  after(:all) do
    Jcb::Sisapo::DB.configure do |config|
      config.username = nil
      config.password = nil
    end
  end

  context "#new" do
    it "Test Class initialize" do
      expect(subject.new).to be_a Jcb::Sisapo::Reuniao
    end
  end

  context ".fetch" do

    ##
    # [{:idReuniao=>42224038, :Reuniao=>38, :Hipodromo=>"RJ", :HipodromoDescricao=>"Gávea", :Dia=>2015-08-08 00:00:00 -0300, :quantidadePareos=>11, :PenetrometroVF=>#<BigDecimal:2dff4d8,'0.41E1',18(18)>, :PenetrometroPR=>#<BigDecimal:2dff460,'0.53E1',18(18)>, :CercaMovel=>#<BigDecimal:2dff190,'0.9E1',9(36)>, :CercaMovelDescricao=>""}
    # {:idReuniao=>42224039, :Reuniao=>39, :Hipodromo=>"SP", :HipodromoDescricao=>"Cidade Jardim", :Dia=>2015-08-08 00:00:00 -0300, :quantidadePareos=>11, :PenetrometroVF=>#<BigDecimal:2dff000,'0.42E1',18(18)>, :PenetrometroPR=>#<BigDecimal:2dfef88,'0.0',9(18)>, :CercaMovel=>nil, :CercaMovelDescricao=>""}
    # {:idReuniao=>42224040, :Reuniao=>40, :Hipodromo=>"SP", :HipodromoDescricao=>"Cidade Jardim", :Dia=>2015-08-08 00:00:00 -0300, :quantidadePareos=>2, :PenetrometroVF=>#<BigDecimal:2dfedf8,'0.42E1',18(18)>, :PenetrometroPR=>#<BigDecimal:2dfed80,'0.0',9(18)>, :CercaMovel=>nil, :CercaMovelDescricao=>""}]
    it "Must fetch list of reunioes" do
      reunioes = subject.fetch(Time.new(2015, 8, 8))

      expect(reunioes.size).to eq(3)
    end

    it "Must convert result to list of Reuniões" do
      rows = [ {:idReuniao=>42225041, :Reuniao=>41, :Hipodromo=>"RJ", :HipodromoDescricao=>"Gávea", :Dia=> DateTime.parse("2015-08-09 00:00:00 -0300"), :quantidadePareos=>12, :PenetrometroVF=> 2.2, :PenetrometroPR=> 4.2, :CercaMovel=> 3.3, :CercaMovelDescricao=>""} ]

      reunioes = subject.convert_to_reunioes(rows)

      reuniao = reunioes.first
      expect(reuniao).to be_a Jcb::Sisapo::Reuniao
      expect(reuniao.id_reuniao).to eq(42225041)
      expect(reuniao.reuniao).to eq(41)
      expect(reuniao.estado).to eq("RJ")
      expect(reuniao.hipodromo).to eq("Gávea")
      expect(reuniao.dia).to eq(DateTime.parse("2015-08-09 00:00:00 -0300"))
      expect(reuniao.quantidade_pareos).to eq(12)
      expect(reuniao.penetrometro_vf).to eq(2.2)
      expect(reuniao.penetrometro_pr).to eq(4.2)
      expect(reuniao.cerca_movel).to eq(3.3)
      expect(reuniao.cerca_movel_descricao).to eq("")
    end
  end
end