require 'jcb'
describe Jcb::Sisapo::Pareo do
  subject { Jcb::Sisapo::Pareo }

  before(:all) do
    Jcb::Sisapo::DB.configure do |config|
      config.username = 'TvTurfe'
      config.password = 'Jockey123'
    end
  end

  after(:all) do
    Jcb::Sisapo::DB.configure do |config|
      config.username = nil
      config.password = nil
    end
  end

  let(:pareo_com_resultado) { {:idReuniao=>42224038, :pareoNumero=>1, :fechado=>true, :pista=>"GM", :distancia=>"1000", :numeroGeral=>190, :mensagemRA=>'RA080815|142528|RJ|Gávea|38|1|GM^1000|101|FINAL|199##5#167#85441#*939##9#501#110811#*55##3#59#111981#*604##8#328#115541#*248##6#409#109211#*464##7#389#115551#*21##2#22#65101#*107##4#131#44131#*16##1#15#216021#^|72930*14430*141870*26710*46730*11150*250310*63450*264660|07-09#22*03-07#52*03-09#68*07-08#116*08-09#143*03-08#159*01-07#208*01-09#222*05-09#268*05-07#288*01-08#309*03-04#318*01-03#329*03-05#459*02-09#496*06-07#657|09-07#33*07-09#63*03-07#99*09-03#124*03-09#128*07-03#132*09-08#145*08-09#158*03-08#227*08-03#233*09-01#254*07-08#318*07-05#425*08-07#426*03-01#431*01-07#479|09-07-03#106*07-09-03#159*09-07-08#177*09-03-07#181*07-09-08#231*03-07-09#248*07-03-09#271*03-09-07#302*09-07-01#335*09-07-05#389*09-03-08#420*07-09-01#435*09-08-07#498*07-09-05#557*03-07-08#592*07-03-08#601|09-07-03-08#454*09-07-08-03#525*07-09-03-08#566*09-07-03-01#624*09-03-07-08#642*07-09-08-03#642*09-07-03-05#703*09-03-08-07#900*07-03-09-08#908*09-07-01-03#909*09-07-08-01#909*07-09-03-01#961*09-07-05-03#979*03-09-07-08#994*09-07-01-08#1012*09-03-07-01#1042|15186610^13597410^10619650#0#0*892240#0#0*511100#0#0*263960#0#0*495540#0#0*814920#0#0|Pick 7$Pick 7$Pick7$Pck7#1127800#0*^Betting 5$Bett 5$Bett5$Btt5#217600#3300000*^Super Betting$SupBETT$SpBet$SBet#243800#3000000*|'} }

  context ".initialize" do
    it "Não deve inicializar o pareo sem reunião e numero" do
      expect {
        subject.new
      }.to raise_error ArgumentError
    end

    it "Deve initializar um pareo passando o numero e a reuniao" do
      pareo = nil

      expect {
        pareo = subject.new 123123, 1
      }.not_to raise_error

      expect(pareo.id_reuniao).to eq(123123)
      expect(pareo.numero).to eq(1)
      expect(pareo.rateios).to eq({})
      expect(pareo.distancia).to be_nil
    end
  end

  context ".fetch_one" do
    it "should fetch one" do
      # byebug
      # subject.fetch 42224038, 1
      # subject.fetch 42722302, 7
    end
  end

  context ".fetch" do

    it "Deve retornar o pareo requisitado" do
      allow(subject).to receive(:pareo_com_resultado).with(42224038, 1) { pareo_com_resultado }

      pareo = subject.fetch 42224038, 1

      expect(pareo).to be_a Jcb::Sisapo::Pareo
      expect(pareo.id_reuniao).to eq(42224038)
      expect(pareo.numero).to eq(1)
      expect(pareo.rateios).to eq(Jcb::Sisapo::Parser.parse(:ra, pareo_com_resultado[:mensagemRA]))
      expect(pareo.distancia).to eq(1000)
      expect(pareo.pista).to eq("GM")
      expect(pareo.status).to eq(:fechado)
      expect(pareo).to be_final
      expect(pareo).not_to be_alinhamento
      expect(pareo).to be_placar
    end

    it "Deve retornar vazio para um páreo que não existe" do
      pareo = subject.fetch 42224038, 12

      expect(pareo).to be_nil
    end
  end

  context ".fetch_current" do
    let(:fetch_current) { {:idReuniao=>42224038, :pareoNumero=>11, :fechado=>true, :pista=>"GM", :distancia=>"1400", :numeroGeral=>200, :mensagemRA=>'RA080815|200250|RJ|Gávea|38|11|GM^1400|100|FINAL|47##3#49#106971#13%*82##8#94#98181#9%*61##4#53#94031#11%*70##6#64#100801#5%*72##7#75#109831#13%*66##5#63#99501#10%*437##10#418#114181#1%*40##1#39#89961#17%*153##9#192#104881#3%*44##2#46#51501#18%^|234172*98492*77264*122420*137230*110655*12225*201775*55180*162700|01-10#89*08-10#97*02-10#100*06-08#134*01-08#135*01-03#153*01-02#169*06-10#181*01-05#201*01-06#204*02-08#212*03-08#212*05-08#215*03-10#224*03-04#225*03-05#279|08-06#145*06-08#173*08-10#179*10-08#180*01-10#194*06-10#222*10-01#242*06-01#297*04-03#299*01-03#325*04-10#330*08-09#334*10-03#356*08-01#359*09-08#365*10-06#377|10-08-01#632*01-08-10#743*08-10-01#802*01-10-08#840*08-01-10#921*10-01-08#1031*10-01-03#1114*08-06-10#1204*08-10-02#1224*08-10-06#1224*08-10-05#1244*01-05-08#1319*08-10-03#1333*08-01-03#1357*01-05-10#1393*08-01-04#1464|10-08-01-03#3404*08-10-01-03#3570*08-10-03-01#3570*10-08-03-01#3909*10-01-03-08#3992*08-01-10-03#4028*10-01-08-03#4116*08-10-01-02#4129*01-08-10-03#4291*08-10-01-05#4347*01-10-08-03#4435*08-01-03-10#4511*01-08-03-10#4590*10-08-01-02#4671*08-03-10-01#4705*10-04-08-03#4705|89820717^8628228^3868310#0#0*1212113#0#0*701355#0#0*373540#0#0*542080#0#0*1930830#0#2000000|Pick 7$Pick 7$Pick7$Pck7#7228500#0*6#204#6#3*5#185#1#1*6##6#3^Betting 5$Bett 5$Bett5$Btt5#1975700#3300000*4#397#1#1*3#331#1#1*4##1#1^Super Betting$SupBETT$SpBet$SBet#4305200#3000000*2#232#85#12*1#144#861#56*2##85#12|'} }

    it "Deve retornar o páreo atual" do
      allow(subject).to receive(:fetch_one).with(42224038, 0) { fetch_current }

      pareo = subject.fetch_current 42224038

      expect(pareo).to be_a Jcb::Sisapo::Pareo
      expect(pareo.id_reuniao).to eq(42224038)
      expect(pareo.numero).to eq(11)
      expect(pareo.rateios).to eq(Jcb::Sisapo::Parser.parse(:ra, fetch_current[:mensagemRA]))
      expect(pareo.distancia).to eq(1400)
      expect(pareo.pista).to eq("GM")
      expect(pareo.status).to eq(:fechado)
      expect(pareo).to be_final
      expect(pareo).not_to be_alinhamento
      expect(pareo).not_to be_placar
    end

    it "Deve retornar o pareo atual sem informar a reuniao" do
      allow(subject).to receive(:fetch_next_one).with(no_args) { {:idReuniao=>42224038} }
      allow(subject).to receive(:fetch_one).with(42224038, 0) { fetch_current }

      pareo = subject.fetch_current

      expect(pareo).to be_a Jcb::Sisapo::Pareo
      expect(pareo.id_reuniao).to eq(42224038)
      expect(pareo.numero).to eq(11)
      expect(pareo.rateios).to eq(Jcb::Sisapo::Parser.parse(:ra, fetch_current[:mensagemRA]))
      expect(pareo.distancia).to eq(1400)
      expect(pareo.pista).to eq("GM")
      expect(pareo.status).to eq(:fechado)
      expect(pareo).to be_final
      expect(pareo).not_to be_alinhamento
      expect(pareo).not_to be_placar
    end
  end

  context ".fetch_next" do
    let(:fetch_current) { {:idReuniao=>42224038, :pareoNumero=>11, :fechado=>true, :pista=>"GM", :distancia=>"1400", :numeroGeral=>200, :mensagemRA=>'RA080815|200250|RJ|Gávea|38|11|GM^1400|100|FINAL|47##3#49#106971#13%*82##8#94#98181#9%*61##4#53#94031#11%*70##6#64#100801#5%*72##7#75#109831#13%*66##5#63#99501#10%*437##10#418#114181#1%*40##1#39#89961#17%*153##9#192#104881#3%*44##2#46#51501#18%^|234172*98492*77264*122420*137230*110655*12225*201775*55180*162700|01-10#89*08-10#97*02-10#100*06-08#134*01-08#135*01-03#153*01-02#169*06-10#181*01-05#201*01-06#204*02-08#212*03-08#212*05-08#215*03-10#224*03-04#225*03-05#279|08-06#145*06-08#173*08-10#179*10-08#180*01-10#194*06-10#222*10-01#242*06-01#297*04-03#299*01-03#325*04-10#330*08-09#334*10-03#356*08-01#359*09-08#365*10-06#377|10-08-01#632*01-08-10#743*08-10-01#802*01-10-08#840*08-01-10#921*10-01-08#1031*10-01-03#1114*08-06-10#1204*08-10-02#1224*08-10-06#1224*08-10-05#1244*01-05-08#1319*08-10-03#1333*08-01-03#1357*01-05-10#1393*08-01-04#1464|10-08-01-03#3404*08-10-01-03#3570*08-10-03-01#3570*10-08-03-01#3909*10-01-03-08#3992*08-01-10-03#4028*10-01-08-03#4116*08-10-01-02#4129*01-08-10-03#4291*08-10-01-05#4347*01-10-08-03#4435*08-01-03-10#4511*01-08-03-10#4590*10-08-01-02#4671*08-03-10-01#4705*10-04-08-03#4705|89820717^8628228^3868310#0#0*1212113#0#0*701355#0#0*373540#0#0*542080#0#0*1930830#0#2000000|Pick 7$Pick 7$Pick7$Pck7#7228500#0*6#204#6#3*5#185#1#1*6##6#3^Betting 5$Bett 5$Bett5$Btt5#1975700#3300000*4#397#1#1*3#331#1#1*4##1#1^Super Betting$SupBETT$SpBet$SBet#4305200#3000000*2#232#85#12*1#144#861#56*2##85#12|'} }

    it "Deve retornar o próximo pareo" do
      allow(subject).to receive(:fetch_current).with(42224038) { subject.new 42224038, 10 }
      allow(subject).to receive(:fetch_one).with(42224038, 11) { fetch_current }

      pareo = subject.fetch_next 42224038

      expect(pareo).to be_a Jcb::Sisapo::Pareo
      expect(pareo.id_reuniao).to eq(42224038)
      expect(pareo.numero).to eq(11)
      expect(pareo.rateios).to eq(Jcb::Sisapo::Parser.parse(:ra, fetch_current[:mensagemRA]))
      expect(pareo.distancia).to eq(1400)
      expect(pareo.pista).to eq("GM")
      expect(pareo.status).to eq(:fechado)
      expect(pareo).to be_final
      expect(pareo).not_to be_alinhamento
      expect(pareo).not_to be_placar
    end
    
    it "Deve retornar nil quando o pareo atual for o ultimo" do
      allow(subject).to receive(:fetch_current).with(42224038) { subject.new 42224038, 11 }
      allow(subject).to receive(:fetch_one).with(42224038, 12) { nil }

      pareo = subject.fetch_next 42224038

      expect(pareo).to be_nil
    end

  end

  context ".fetch_prev" do

    it "Deve retornar nil quando o pareo atual for o primeiro pareo" do
      allow(subject).to receive(:fetch_current).with(42224038) { subject.new 42224038, 1 }

      pareo = subject.fetch_prev 42224038

      expect(pareo).to be_nil
    end
  end

  context "#resultado" do
    let(:com_resultado) {
      [
        {:Estado=>"C", :idReuniao=>42224038, :NumeroPareo=>1, :OrdemDeChegada=>"03-09-04-08-05", :Favoritismo=>"09-07-03-08-01-05-06-04-02", :TempoDoVencedor=>"56s74", :MovimentoPareo=> 86282.28, :RetiradosDescricao=>nil, :tipoAposta=>"1", :Indicacao=>"C", :Complemento=>"", :IndicacaoDescricao=>"03", :Rateio=> 7.2, :Acrescimo=> 0.0, :tipoApostaDescricao=>"Vencedor"},
        {:Estado=>"C", :idReuniao=>42224038, :NumeroPareo=>1, :OrdemDeChegada=>"03-09-04-08-05", :Favoritismo=>"09-07-03-08-01-05-06-04-02", :TempoDoVencedor=>"56s74", :MovimentoPareo=> 86282.28, :RetiradosDescricao=>nil, :tipoAposta=>"2", :Indicacao=>"C", :Complemento=>"", :IndicacaoDescricao=>"03", :Rateio=> 3.2, :Acrescimo=> 0.0, :tipoApostaDescricao=>"Placê"},
        {:Estado=>"C", :idReuniao=>42224038, :NumeroPareo=>1, :OrdemDeChegada=>"03-09-04-08-05", :Favoritismo=>"09-07-03-08-01-05-06-04-02", :TempoDoVencedor=>"56s74", :MovimentoPareo=> 86282.28, :RetiradosDescricao=>nil, :tipoAposta=>"2", :Indicacao=>"I", :Complemento=>"", :IndicacaoDescricao=>"09", :Rateio=> 2.5, :Acrescimo=> 0.0, :tipoApostaDescricao=>"Placê"},
        {:Estado=>"C", :idReuniao=>42224038, :NumeroPareo=>1, :OrdemDeChegada=>"03-09-04-08-05", :Favoritismo=>"09-07-03-08-01-05-06-04-02", :TempoDoVencedor=>"56s74", :MovimentoPareo=> 86282.28, :RetiradosDescricao=>nil, :tipoAposta=>"3", :Indicacao=>"CI", :Complemento=>"", :IndicacaoDescricao=>"03-09", :Rateio=> 21.5, :Acrescimo=> 0.0, :tipoApostaDescricao=>"Dupla"},
        {:Estado=>"C", :idReuniao=>42224038, :NumeroPareo=>1, :OrdemDeChegada=>"03-09-04-08-05", :Favoritismo=>"09-07-03-08-01-05-06-04-02", :TempoDoVencedor=>"56s74", :MovimentoPareo=> 86282.28, :RetiradosDescricao=>nil, :tipoAposta=>"4", :Indicacao=>"CI", :Complemento=>"", :IndicacaoDescricao=>"03-09", :Rateio=> 64.0, :Acrescimo=> 0.0, :tipoApostaDescricao=>"Exata"},
        {:Estado=>"C", :idReuniao=>42224038, :NumeroPareo=>1, :OrdemDeChegada=>"03-09-04-08-05", :Favoritismo=>"09-07-03-08-01-05-06-04-02", :TempoDoVencedor=>"56s74", :MovimentoPareo=> 86282.28, :RetiradosDescricao=>nil, :tipoAposta=>"5", :Indicacao=>"CID", :Complemento=>"", :IndicacaoDescricao=>"03-09-04", :Rateio=> 240.8, :Acrescimo=> 0.0, :tipoApostaDescricao=>"Trifeta"},
        {:Estado=>"C", :idReuniao=>42224038, :NumeroPareo=>1, :OrdemDeChegada=>"03-09-04-08-05", :Favoritismo=>"09-07-03-08-01-05-06-04-02", :TempoDoVencedor=>"56s74", :MovimentoPareo=> 86282.28, :RetiradosDescricao=>nil, :tipoAposta=>"6", :Indicacao=>"CIDH", :Complemento=>"", :IndicacaoDescricao=>"03-09-04-08", :Rateio=> 1882.0, :Acrescimo=> 0.0, :tipoApostaDescricao=>"Quadrifeta"}
      ]
    }

    it "Deve retorna os resultados de um pareo finalizado" do
      allow(subject).to receive(:fetch_one).with(42224038, 1) { pareo_com_resultado }

      pareo = subject.fetch 42224038, 1

      allow(pareo).to receive(:resultado) { Jcb::Sisapo::Resultado.fetch(42224038, 1)  }

      resultado = pareo.resultado

      expect(resultado).to be_a Jcb::Sisapo::Resultado
      expect(resultado.id_reuniao).to eq(42224038)
      expect(resultado.numero).to eq(1)
      expect(resultado.estado).to eq("C")
      expect(resultado.resultados[:favoritismo]).to eq('09-07-03-08-01-05-06-04-02')
      expect(resultado.resultado_para(:vencedor).first[:numero]).to eq('03')
      expect(resultado.resultado_para(:place).first[:numero]).to eq('03')
      expect(resultado.resultado_para(:place).last[:numero]).to eq('09')
      expect(resultado.resultado_para(:dupla).first[:numero]).to eq('03-09')
      expect(resultado.resultado_para(:exata).first[:numero]).to eq('03-09')
      expect(resultado.resultado_para(:trifeta).first[:numero]).to eq('03-09-04')
      expect(resultado.resultado_para(:quadrifeta).first[:numero]).to eq('03-09-04-08')
    end
  end

  it ".pareo_extras" do
    pareo = subject.fetch 42231046, 10
  end
end