describe Jcb::Sisapo::Resultado do
  subject { Jcb::Sisapo::Resultado }
  
  before(:all) do
    Jcb::Sisapo::DB.configure do |config|
      config.username = 'TvTurfe'
      config.password = 'Jockey123'
    end
  end

  after(:all) do
    Jcb::Sisapo::DB.configure do |config|
      config.username = nil
      config.password = nil
    end
  end

  let(:com_resultado) {
    [
      {:Estado=>"C", :idReuniao=>42224038, :NumeroPareo=>1, :OrdemDeChegada=>"03-09-04-08-05", :Favoritismo=>"09-07-03-08-01-05-06-04-02", :TempoDoVencedor=>"56s74", :MovimentoPareo=> 86282.28, :RetiradosDescricao=>nil, :tipoAposta=>"1", :Indicacao=>"C", :Complemento=>"", :IndicacaoDescricao=>"03", :Rateio=> 7.2, :Acrescimo=> 0.0, :tipoApostaDescricao=>"Vencedor"},
      {:Estado=>"C", :idReuniao=>42224038, :NumeroPareo=>1, :OrdemDeChegada=>"03-09-04-08-05", :Favoritismo=>"09-07-03-08-01-05-06-04-02", :TempoDoVencedor=>"56s74", :MovimentoPareo=> 86282.28, :RetiradosDescricao=>nil, :tipoAposta=>"2", :Indicacao=>"C", :Complemento=>"", :IndicacaoDescricao=>"03", :Rateio=> 3.2, :Acrescimo=> 0.0, :tipoApostaDescricao=>"Placê"},
      {:Estado=>"C", :idReuniao=>42224038, :NumeroPareo=>1, :OrdemDeChegada=>"03-09-04-08-05", :Favoritismo=>"09-07-03-08-01-05-06-04-02", :TempoDoVencedor=>"56s74", :MovimentoPareo=> 86282.28, :RetiradosDescricao=>nil, :tipoAposta=>"2", :Indicacao=>"I", :Complemento=>"", :IndicacaoDescricao=>"09", :Rateio=> 2.5, :Acrescimo=> 0.0, :tipoApostaDescricao=>"Placê"},
      {:Estado=>"C", :idReuniao=>42224038, :NumeroPareo=>1, :OrdemDeChegada=>"03-09-04-08-05", :Favoritismo=>"09-07-03-08-01-05-06-04-02", :TempoDoVencedor=>"56s74", :MovimentoPareo=> 86282.28, :RetiradosDescricao=>nil, :tipoAposta=>"3", :Indicacao=>"CI", :Complemento=>"", :IndicacaoDescricao=>"03-09", :Rateio=> 21.5, :Acrescimo=> 0.0, :tipoApostaDescricao=>"Dupla"},
      {:Estado=>"C", :idReuniao=>42224038, :NumeroPareo=>1, :OrdemDeChegada=>"03-09-04-08-05", :Favoritismo=>"09-07-03-08-01-05-06-04-02", :TempoDoVencedor=>"56s74", :MovimentoPareo=> 86282.28, :RetiradosDescricao=>nil, :tipoAposta=>"4", :Indicacao=>"CI", :Complemento=>"", :IndicacaoDescricao=>"03-09", :Rateio=> 64.0, :Acrescimo=> 0.0, :tipoApostaDescricao=>"Exata"},
      {:Estado=>"C", :idReuniao=>42224038, :NumeroPareo=>1, :OrdemDeChegada=>"03-09-04-08-05", :Favoritismo=>"09-07-03-08-01-05-06-04-02", :TempoDoVencedor=>"56s74", :MovimentoPareo=> 86282.28, :RetiradosDescricao=>nil, :tipoAposta=>"5", :Indicacao=>"CID", :Complemento=>"", :IndicacaoDescricao=>"03-09-04", :Rateio=> 240.8, :Acrescimo=> 0.0, :tipoApostaDescricao=>"Trifeta"},
      {:Estado=>"C", :idReuniao=>42224038, :NumeroPareo=>1, :OrdemDeChegada=>"03-09-04-08-05", :Favoritismo=>"09-07-03-08-01-05-06-04-02", :TempoDoVencedor=>"56s74", :MovimentoPareo=> 86282.28, :RetiradosDescricao=>nil, :tipoAposta=>"6", :Indicacao=>"CIDH", :Complemento=>"", :IndicacaoDescricao=>"03-09-04-08", :Rateio=> 1882.0, :Acrescimo=> 0.0, :tipoApostaDescricao=>"Quadrifeta"}
    ]
  }

  let(:sem_resultado) { [] }

  context ".fetch" do
    it "Deve retornar os resultados de um páreo finalizado" do
      allow(subject).to receive(:fetch_one).with(42224038, 1) { com_resultado }

      resultado = subject.fetch 42224038, 1

      expect(resultado).to be_a Jcb::Sisapo::Resultado
      expect(resultado.id_reuniao).to eq(42224038)
      expect(resultado.numero).to eq(1)
      expect(resultado.estado).to eq("C")
      expect(resultado.resultados[:favoritismo]).to eq('09-07-03-08-01-05-06-04-02')
      expect(resultado.resultado_para(:vencedor).first[:numero]).to eq('03')
      expect(resultado.resultado_para(:place).first[:numero]).to eq('03')
      expect(resultado.resultado_para(:place).last[:numero]).to eq('09')
      expect(resultado.resultado_para(:dupla).first[:numero]).to eq('03-09')
      expect(resultado.resultado_para(:exata).first[:numero]).to eq('03-09')
      expect(resultado.resultado_para(:trifeta).first[:numero]).to eq('03-09-04')
      expect(resultado.resultado_para(:quadrifeta).first[:numero]).to eq('03-09-04-08')
    end

  end
end

# 42175363/4º empate para primeiro
# 42161349/12º empate para quinto (com faixa)
# 42131320/3º empate para segundo